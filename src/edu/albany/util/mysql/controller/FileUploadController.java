package edu.albany.util.mysql.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.ResponseStatus;


/**
 * Handles requests for the application file upload requests
 */
@Controller
public class FileUploadController {

	@Autowired
	ServletContext context; 

	 
	private static final Logger logger = LoggerFactory
			.getLogger(FileUploadController.class);

	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	CustomResponse uploadFileHandler(@RequestParam("uploadFile") MultipartFile file) {
		String output = storeFile(file,file.getOriginalFilename());
		CustomResponse cr = new CustomResponse();
		cr.setCode(ResponseStatus.success);
		cr.setDesc("file path is :"+ output);
		cr.setObj(output);
		return cr;
	}


	/**
	 * Read file, convert file into string return to client.
	 */
	@RequestMapping(value = "/readFile", method = RequestMethod.GET)
	public @ResponseBody
	CustomResponse ReadFile(@RequestParam("path") String FILENAME) {
		BufferedReader br = null;
		FileReader fr = null;
		StringBuilder output = new StringBuilder();

		try {

			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);

			String sCurrentLine;

			br = new BufferedReader(new FileReader(FILENAME));

			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				output.append(sCurrentLine);
			}
			
		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {
				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();
			}

		}
		
		CustomResponse cr = new CustomResponse();
		cr.setCode(ResponseStatus.success);
		cr.setDesc("Get file content");
		cr.setObj(output.toString());
		return cr;

	}

	
	/**
	 * Store file
	 */
	public String storeFile(MultipartFile file, String fileName){
		
		// new a random filename
		Random rn = new Random();
		int rnum = rn.nextInt(10) + 1;
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String name = dateFormat.format(date).toString()+String.valueOf(rnum)+fileName;
		
		
		
		if (!file.isEmpty()) {
			try {
				// location to store file
				String uploadPath = context.getRealPath("//resources")+"/data/";
				
				// Upload file
				byte[] bytes = file.getBytes();
				File dir = new File(uploadPath);
				if (!dir.exists())
					dir.mkdirs();
				System.out.println(uploadPath);
				
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());

				return "@uploaded file @ " + name;
			} catch (Exception e) {
				return "@You failed to upload " + name + " => " + e.getMessage();
			}
		} else {
			return "@You failed to upload " + name+ " because the file was empty.";
		}
	}
	

	public static void main(String arg[]) throws InterruptedException{
	}
	
	
}

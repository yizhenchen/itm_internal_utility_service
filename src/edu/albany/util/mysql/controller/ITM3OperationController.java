package edu.albany.util.mysql.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.albany.util.mysql.dao.Note;
import edu.albany.util.mysql.dao.Project;
import edu.albany.util.mysql.dao.RequestToGetThreadView;
import edu.albany.util.mysql.dao.RequestToITM3;
import edu.albany.util.mysql.dao.User;
import edu.albany.util.mysql.dao.View;
import edu.albany.util.mysql.dao.requestRegisterCommunity;
import edu.albany.util.mysql.dao.requestToGetUserToken;
import edu.albany.util.mysql.service.ITM3Operation;
import edu.albany.util.mysql.service.KFSearchService;
import edu.albany.util.mysql.service.SqlService;
import edu.albany.util.mysql.service.UserService;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;
import edu.albany.util.mysql.utility.ResponseStatus;
import edu.albany.util.mysql.utility.ResponseUtil;


@RestController
@RequestMapping("/")
public class ITM3OperationController {
	
	final static Logger logger = Logger.getLogger(ITM3OperationController.class);
	
	@Autowired
	public DatabaseUtils db;
	
	@Autowired
	public ResponseUtil responseUtil;
	
	@Resource
	public UserService userService;
	
	@Resource
	private SqlService Service;
	
	@Resource
	public ITM3Operation ITM3Service;
	
	@Resource
	public KFSearchService searchService;

	
		@CrossOrigin
		@RequestMapping(value = "/project/get_by_user_id", method = RequestMethod.POST, consumes = {"application/json" })
		public  CustomResponse getProject(@RequestBody  RequestToITM3 requestJson
				){
			db.setURL("itm3");
			// get localdb from itm3 by community_id
			String localdb = ITM3Service.getLocalDb(requestJson.getCommunity_id());
			if(localdb.equals(null)){
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.failure);
				cr.setDesc("The database has not been registered into ITM3");
				cr.setObj(true);
				return cr;
			}
			
			db.setURL(localdb);
			// get notes and user info belong to the view by view id
			List<Project> projects = ITM3Service.getProjectsByUserId(requestJson.getKf_user_id());
			
			if(null==projects||projects.get(0).getId()==0){
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.failure);
				cr.setDesc("The user does not have any project .");
				cr.setObj(true);
				return cr;
			}
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("");
			cr.setObj(projects);
			return cr;
		}
		
		
		
		@CrossOrigin
		@RequestMapping(value = "/community/add", method = RequestMethod.POST, consumes = {"application/json" })
		public  CustomResponse addCommunity(@RequestBody  requestRegisterCommunity requestRegisterCommunity ) throws IOException{
			db.setURL("itm3");
			String localdb =ITM3Service.getLocalDb(requestRegisterCommunity.getCommunity_id());
			
			if(null==localdb||localdb.equals("")){
				// if community does not exist then create database in ITM
				String dbname =StringUtils.join(requestRegisterCommunity.getCommunity_name().replace("\'", "").replace("-", "_").replace("\"", "").split(" "), "_");
				ITM3Service.createDatabase(dbname,null);
				ITM3Service.importKFdata(
						requestRegisterCommunity.getCommunity_url(),
						requestRegisterCommunity.getToken(),
						dbname
						);
				ITM3Service.addCommunity(requestRegisterCommunity);
			}else{
				// if community already exist , then update database
				ITM3Service.updateDatebase(localdb);
			}
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success); 
			cr.setDesc("");
			cr.setObj(null);
			return cr;
		}
		
		

		@CrossOrigin
		@RequestMapping(value = "/thread/view/get", method = RequestMethod.POST, consumes = {"application/json" })
		public  CustomResponse threadViewGet(
				@RequestBody RequestToGetThreadView requestToGetThreadView
				){ 
			db.setURL("itm3");
			String localdb =ITM3Service.getLocalDb(requestToGetThreadView.getCommunity_id());
			if(null==localdb||localdb.equals(null)){
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.failure);
				cr.setDesc("The database has not been registered into ITM3");
				cr.setObj(true);
				return cr;
			}
			
			db.setURL(localdb);
			List<View> views= ITM3Service.getViewsByThreadId(requestToGetThreadView.getThread_id());
			
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("");
			cr.setObj(views);
			return cr;
		}
		
		
		

		
		@CrossOrigin
		@RequestMapping(value = "/project/get_by_note_id", method = RequestMethod.POST , consumes = {"application/json" })
		public  CustomResponse getProjectByNoteId(@RequestBody RequestToITM3 requestJson
				){
			db.setURL("itm3");
			// get localdb from itm3 by community_id
			String localdb = ITM3Service.getLocalDb(requestJson.getCommunity_id());
			if(localdb.equals(null)){
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.failure);
				cr.setDesc("The database has not been registered into ITM3");
				cr.setObj(true);
				return cr;
			}
			
			db.setURL(localdb);
			// get notes and user info belong to the view by view id
			
			List<String> note_ids = new ArrayList<String>();
			note_ids.add(requestJson.getNote_id());
			List<Project> projects=ITM3Service.getProjectByNotes(note_ids);
			if(projects.isEmpty()){
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.failure);
				cr.setDesc("This note does not belong to any project. ");
				cr.setObj(true);
				return cr;
			}
			
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("");
			cr.setObj(projects);
			return cr;
		}
		
	

		@CrossOrigin
		@RequestMapping(value = "/thread/note/add", method = RequestMethod.POST , consumes = {"application/json" })
		public  CustomResponse addNoteIntoThread(@RequestBody RequestToITM3 requestJson
				){
			
			db.setURL("itm3");
			// get localdb from itm3 by community_id
			String localdb = ITM3Service.getLocalDb(requestJson.getCommunity_id());
			if(localdb.equals(null)){
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.failure);
				cr.setDesc("The database have not been registered into ITM3");
				cr.setObj(true);
				return cr;
			}
			db.setURL(localdb);
			// add note into note table 
			Note note = new Note();
			note.setContent(requestJson.getContent());
			note.setCreate_time(requestJson.getCreate_time());
			note.setTitle(requestJson.getTitle());
			note.setModified_time(requestJson.getModified_time());
			note.setNote_id(requestJson.getNote_id());
			
			// check if note exist or not
			
			ITM3Service.addNoteIntoNoteTable(note);
			
			// add note thread information
			
			if(null!=requestJson.getAuthors()){
				for(String id : requestJson.getAuthors()){
					ITM3Service.addNoteAuthorInfo(id, requestJson.getNote_id());
				}
			}
			//ITM3Service.addNoteAuthorInfo(requestJson.getKf_user_id(), requestJson.getNote_id());
			
			// add note author information
			ITM3Service.addNoteIntoThreadTable(requestJson.getThread_id(),  requestJson.getNote_id());

			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("");
			return cr;
		}
	

		


		@CrossOrigin
		@RequestMapping(value = "/note/add", method = RequestMethod.POST , consumes = {"application/json" })
		public  CustomResponse addNote(@RequestBody RequestToITM3 requestJson
				){
			
			db.setURL("itm3");
			// get localdb from itm3 by community_id
			String localdb = ITM3Service.getLocalDb(requestJson.getCommunity_id());
			if(localdb.equals(null)){
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.failure);
				cr.setDesc("The database have not been registered into ITM3");
				cr.setObj(true);
				return cr;
			}
			db.setURL(localdb);
			// add note into note table 
			Note note = new Note();
			note.setContent(requestJson.getContent());
			note.setCreate_time(requestJson.getCreate_time());
			note.setTitle(requestJson.getTitle());
			note.setModified_time(requestJson.getModified_time());
			note.setNote_id(requestJson.getNote_id());
			
			// check if note exist or not
			if(ITM3Service.checkNoteId(requestJson.getNote_id())){ // this note already in the database 
				// update the note
				// add note into database
				ITM3Service.updateNote(note);
				// delete co-author
				ITM3Service.deleteNoteAuthor(note);
				// add note thread information
				if(null!=requestJson.getAuthors()){
					for(String id : requestJson.getAuthors()){
						ITM3Service.addNoteAuthorInfo(id, requestJson.getNote_id());
					}
				}
				
			}else{
				// add note into database
				ITM3Service.addNoteIntoNoteTable(note);
				// add note thread information
				ITM3Service.addNoteAuthorInfo(requestJson.getKf_user_id(), requestJson.getNote_id());
				if(null!=requestJson.getAuthors()){
					for(String id : requestJson.getAuthors()){
						ITM3Service.addNoteAuthorInfo(id, requestJson.getNote_id());
					}
				}
			}

			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("");
			return cr;
		}
	
		
		@CrossOrigin
		@RequestMapping(value = "/user/check_by_token", method = RequestMethod.POST)
		public boolean checkUser2(
				@RequestParam(value="token", required=false) String token,
				@RequestParam(value="kfurl", required=false) String kfurl,
				@RequestParam(value="community_id", required=false) String community_id
			){
			return ITM3Service.checkUser(token,kfurl,community_id);
		}
		
		
		@CrossOrigin
		@RequestMapping(value = "/kf/user/token", method = RequestMethod.POST)
		public  CustomResponse getUserToken(
				 @RequestBody requestToGetUserToken request
				)  {
			db.setURL("itm3");
			User kfuser = null;
			try {
				kfuser = ITM3Service.getUser(request.getCommunity_url(),request.getToken(),request.getCommunity_id());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(null!=kfuser){
				// get localdb
				String localdb = ITM3Service.getLocalDb(request.getCommunity_id());
				if(null==localdb){
					CustomResponse cr = new CustomResponse();
					cr.setCode(ResponseStatus.failure);
					cr.setDesc("The database have not been registered into ITM3");
					cr.setObj(true);
					return cr;
				}
				db.setURL(localdb);
				kfuser.setLocalDatabases(localdb);
				User user =ITM3Service.transfortToITMUser(kfuser);
				
				if(!ITM3Service.getUserByStrId(kfuser.getStr_id())){
					//add kf user 
					db.setURL("itm3");
					Random rand = new Random();
					int  n = rand.nextInt(1000) + 1;
					kfuser.setToken(ITM3Service.getToken(n));
					user =ITM3Service.transfortToITMUser(kfuser);
					ITM3Service.addUserInfoIntoITM(user);
					ITM3Service.addUserCommunityInfoIntoITM(user);
					db.setURL(localdb);
					ITM3Service.addUserInfoInToCommunity(user);
				}else{
					user.setToken(ITM3Service.getUserTokenByStrId(kfuser.getStr_id()));
				}
				
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.success);
				cr.setDesc("");
				cr.setObj(user);
				return cr;
				
			}else{
				
				CustomResponse cr = responseUtil.fail();
				cr.setDesc("Invaild Token ");
				return cr;
			}
			
			
		}
		
		
	public static void main(String arg[]){
		 
	}

}



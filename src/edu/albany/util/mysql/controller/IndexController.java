package edu.albany.util.mysql.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.albany.util.mysql.dao.Note;
import edu.albany.util.mysql.dao.Project;
import edu.albany.util.mysql.service.ITM3Operation;
import edu.albany.util.mysql.service.KFSearchService;
import edu.albany.util.mysql.service.SqlService;
import edu.albany.util.mysql.service.UserService;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;
import edu.albany.util.mysql.utility.ResponseStatus;
import edu.albany.util.mysql.utility.ResponseUtil;


@RestController
@RequestMapping("/util/")
public class IndexController {
	
	final static Logger logger = Logger.getLogger(IndexController.class);
	
	@Autowired
	public DatabaseUtils db;
	
	@Autowired
	public ResponseUtil responseUtil;
	
	@Resource
	public UserService userService;
	
	@Resource
	public SqlService Service;
	
	@Resource
	public ITM3Operation ITM3Service;
	
	@Resource
	public KFSearchService searchService;

	
	@CrossOrigin
	@RequestMapping(value = "/userValidate", method = RequestMethod.POST)
	public  CustomResponse UserValidation(@RequestParam(value="token", required=false) String token)  {
		db.setURL("itm3");
		if(!userService.CheckToken(token)){// if user not appear in database,then validation fails
			CustomResponse cr = responseUtil.fail();
			cr.setDesc("Invalid User");
			return cr;
		}else{
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("Valid User");
			cr.setObj(true);
			return cr;
		}
		
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	public  void sendEmail(@RequestParam(value="email", required=false) String email,
			@RequestParam(value="message", required=false) String msg,
			@RequestParam(value="title", required=false) String title
			)  {
		 final String username = "itm.test.send@gmail.com";
	      final String password = "U@lbany9";
				Properties props = new Properties();
				    props.put("mail.smtp.user", username);
				    props.put("mail.smtp.host", "smtp.gmail.com");
				    props.put("mail.smtp.port", "25");
				    props.put("mail.debug", "true");
				    props.put("mail.smtp.auth", "true");
				    props.put("mail.smtp.starttls.enable", "true");
				    props.put("mail.smtp.EnableSSL.enable", "true");
				    props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				    props.setProperty("mail.smtp.socketFactory.fallbac k", "false");
				    props.setProperty("mail.smtp.port", "465");
				    props.setProperty("mail.smtp.socketFactory.port", "465");
		         Session session = Session.getInstance(props,
		         new javax.mail.Authenticator() {
		             protected PasswordAuthentication getPasswordAuthentication() {
		                 return new PasswordAuthentication(username, password);
		             }
		         });


		         
		         try {
		             Message message = new MimeMessage(session);
		             message.setFrom(new InternetAddress(username));
		             message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
		             message.setSubject(title);
		             message.setText(msg);
		             Transport.send(message);
		         	}catch (MessagingException mex) {
			         mex.printStackTrace();
			      }
	}

	
	@CrossOrigin
	@RequestMapping(value = "/ext_sendEmail", method = RequestMethod.POST)
	public  void sendEmail2(@RequestParam(value="email", required=false) String email,
			@RequestParam(value="message", required=false) String msg,
			@RequestParam(value="title", required=false) String title
			)  {
		 final String username = "midealoftheweek@gmail.com";
	      final String password = "123456asdfgh";
				Properties props = new Properties();
				    props.put("mail.smtp.user", username);
				    props.put("mail.smtp.host", "smtp.gmail.com");
				    props.put("mail.smtp.port", "25");
				    props.put("mail.debug", "true");
				    props.put("mail.smtp.auth", "true");
				    props.put("mail.smtp.starttls.enable", "true");
				    props.put("mail.smtp.EnableSSL.enable", "true");
				    props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				    props.setProperty("mail.smtp.socketFactory.fallbac k", "false");
				    props.setProperty("mail.smtp.port", "465");
				    props.setProperty("mail.smtp.socketFactory.port", "465");
		         Session session = Session.getInstance(props,
		         new javax.mail.Authenticator() {
		             protected PasswordAuthentication getPasswordAuthentication() {
		                 return new PasswordAuthentication(username, password);
		             }
		         });


		         
		         try {
		             Message message = new MimeMessage(session);
		             message.setContent(msg, "text/html; charset=utf-8");
		             message.setFrom(new InternetAddress(username));
		             message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
		             message.setSubject(title);
		             Transport.send(message);
		         	}catch (MessagingException mex) {
			         mex.printStackTrace();
			      }
	}

	
	@CrossOrigin
	@RequestMapping(value = "/permissionValidate", method = RequestMethod.POST)
	public  CustomResponse validatePermission(HttpServletRequest request,  HttpServletResponse response,
			@RequestParam(value="serviceId", required=false) int serviceId,
			@RequestParam(value="token", required=false) String token)  {
		db.setURL("itm3");
		if(!userService.CheckPermission(serviceId,token)){
			// if serviceId and token not paired in database,then validation fails
			CustomResponse cr = responseUtil.fail();
			cr.setDesc("Permission denial.");
			return cr;
		}else{
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("Permission permit.");
			cr.setObj(true);
			return cr;
		}
		
	}
	
	
	
	// Generate token
	@CrossOrigin
	@RequestMapping(value = "/getToken", method = RequestMethod.GET)
	public  CustomResponse validatePermission(HttpServletRequest request,  HttpServletResponse response,
			@RequestParam(value="uid", required=false) int uid)  {
		String token = userService.getToken(uid);
		CustomResponse cr = new CustomResponse();
		cr.setCode(ResponseStatus.success);
		cr.setDesc("Get token .");
		cr.setObj(token);
		return cr;
		
	}
	
	// Generate access code
	CustomResponse cr = new CustomResponse();

	@CrossOrigin
	@RequestMapping(value = "/getAccessCode", method = RequestMethod.GET)
	public  CustomResponse getAccessCode(HttpServletRequest request,  HttpServletResponse response,
			@RequestParam(value="uid", required=false) int uid)  {
		String token = userService.getToken(uid);
		if(null!=token){
		cr.setCode(ResponseStatus.success);
		cr.setDesc("Get token .");
		cr.setObj(token);
		}
		return cr;
		
	}
	
	// set up all user's token
	@CrossOrigin
	@RequestMapping(value = "/setUsersToken", method = RequestMethod.GET)
	public  CustomResponse setUserToken(HttpServletRequest request,  HttpServletResponse response,
			@RequestParam(value="database", required=false) String database){
		db.setURL(database);
		userService.setUserToken();
		cr.setCode(ResponseStatus.success);
		cr.setDesc("Set token done.");
		cr.setObj(true);
		return cr;
	}
	
	
	// search by 
	@CrossOrigin
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public  CustomResponse search(
			HttpServletRequest request,  HttpServletResponse response,
			@RequestParam(value="database", required=false) String database,
			@RequestParam(value="view_ids", required=false) String view_ids,
			@RequestParam(value="project_ids", required=false) String project_ids,
			@RequestParam(value="from_date", required=false) String from_date,
			@RequestParam(value="to_date", required=false) String to_date,
			@RequestParam(value="category", required=false) String category,
			@RequestParam(value="keywords", required=false) String keywords,
			@RequestParam(value="isExact", required=false) String isExact,
			@RequestParam(value="users", required=false) String users
			){
		db.setURL(database);
		CustomResponse cr = new CustomResponse();
		cr.setCode(ResponseStatus.success);
		cr.setObj(searchService.search(view_ids, project_ids, from_date, to_date, isExact,category,keywords,users));
		return cr;
	}
	
	
	
	
	public static void main(String arg[]){
		 
	}

}



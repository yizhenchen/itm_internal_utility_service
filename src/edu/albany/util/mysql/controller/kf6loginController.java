package edu.albany.util.mysql.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.albany.util.mysql.dao.Community;
import edu.albany.util.mysql.service.CommunityService;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;
import edu.albany.util.mysql.utility.ResponseStatus;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import com.sun.jersey.core.util.MultivaluedMapImpl;



@RestController
@RequestMapping("/kf6/")
public class kf6loginController {

	@Autowired
	public DatabaseUtils db;
	
	@Resource
	public CommunityService communityService;
	
	
	//get kf communites and local communites
	@CrossOrigin
	@RequestMapping(value = "/getCommunities", method = RequestMethod.POST)
	public CustomResponse get(
			@RequestParam("token") String token,
			@RequestParam(value="kfurl", required=false) String kfurl){
		db.setURL("itm3");
		CustomResponse output = new CustomResponse();
		List<Community> communities = new ArrayList<Community>();
		String uid="";
		String json =getkfCommunities(token,kfurl).getCode().equals(ResponseStatus.success)?getkfCommunities(token,kfurl).getObj().toString():"";
		communities=getKfCommunitiesFromJson(json) ;
		// get local communites from db
		List<Community> localCommunities = communityService.getLocalComunities();
		
		
		// get my local communities
		List<Community> myCommunities = new ArrayList<Community>();
		if(!communities.isEmpty()){
			myCommunities = communityService.getLocalComunities(communities.get(0).getStr_id());
		}
		
		// check if the user's kf communities has been loaded. 
		for(Community lc :localCommunities ){
			boolean exist =false;
			if(null!=lc.getCommunity()&&""!=lc.getCommunity()&&null!=lc.getCommunity_id()){
				for(Community cc:communities){
					if(null!=cc.getCommunity()){
						if(cc.getCommunity_id().equals(lc.getCommunity_id().trim())){
							cc.setLocaldb(lc.getLocaldb());
							cc.setITM(true);
							exist=true;
							break;
						}
					}			
				}
				if(!exist){
					communities.add(lc);
				}
			}
		}
		
		
		
		// check if the user's local communities 
		for(Community lc :myCommunities ){
			boolean exist =false;
			if(null!=lc.getCommunity()||""!=lc.getCommunity()){
				// check if the local communities  is  belongs to the user
				for(Community cc:communities){  //  yes, setITM is true and add localdb info
					if(null!=lc.getCommunity()&&""!=lc.getCommunity()&&null!=lc.getCommunity_id()){
						if(cc.getCommunity_id().equals(lc.getCommunity_id())){
							cc.setLocaldb(lc.getLocaldb());
							cc.setITM(true);
							exist=true;
							break;
						}
					}			
				}
				if(!exist){// no
					communities.add(lc);
				}
			}
		}
		
		
		if(communities.isEmpty()){
			output.setCode(ResponseStatus.failure);
    		output.setDesc("No KF Community has been add into ITM");
		}else{
			
			output.setCode(ResponseStatus.success);
			output.setObj(communities);
		}
		return output;
	}
	
	
	
	
	
	// get local communites
	@CrossOrigin
	@RequestMapping(value = "/getLocalCommunities", method = RequestMethod.POST)
	public CustomResponse getLocalCommunities(
			@RequestParam(value="email", required=false) String email,
			@RequestParam(value="password", required=false) String password){
			db.setURL("itm3");
			CustomResponse output = new CustomResponse();
			List<Community> localCommunitiesofUser = communityService.getLocalComunities(email,password);
			List<Community> localCommunities = communityService.getLocalComunities();
		
			for(Community lc :localCommunitiesofUser ){
				boolean exist =false;
				if(null!=lc.getCommunity()&&""!=lc.getCommunity()&&null!=lc.getCommunity_id()){
					for(Community cc:localCommunities){
						if(null!=cc.getCommunity()&&null!=cc.getCommunity_id()){
							if(cc.getCommunity_id().equals(lc.getCommunity_id())){
								cc.setEmail(email);
								cc.setUserName(lc.getUserName());
								cc.setLocaldb(lc.getLocaldb());
								cc.setStr_id(lc.getStr_id());
								cc.setRole(lc.getRole());
								cc.setITM(true);
							}
						}
						
					}
				}
				
				
			}
			if(localCommunities.isEmpty()){
				output.setCode(ResponseStatus.failure);
	    		output.setDesc("No KF Community has been add into ITM");
			}else{
				
				output.setCode(ResponseStatus.success);
				output.setObj(localCommunities);
			}
		
		return output;
	}
	
    //  check user info
	@CrossOrigin
	@RequestMapping(value = "/checkuser", method = RequestMethod.POST)
	public CustomResponse checkUser(
			@RequestParam(value="username", required=false) String username,
			@RequestParam(value="password", required=false) String password,
			@RequestParam(value="kfurl", required=false) String kfurl){
		CustomResponse output = new CustomResponse();
		Client client = getClient();
		if(!kfurl.endsWith("/")){
			kfurl=kfurl+"/";
		}
		
		WebResource webResource = client.resource(kfurl+"auth/local");
	    
	    MultivaluedMap formData = new MultivaluedMapImpl();
	    formData.add("userName", username);
	    formData.add("password", password);
	    ClientResponse response = webResource.accept("application/json").type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
	    InputStream in =response.getEntityInputStream();
	    StringWriter writer = new StringWriter();
	    
	    try {
			IOUtils.copy(in, writer, "utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    String theString = writer.toString();
	    System.out.println(theString);
	    if(response.getStatus()!=200){
	    	output.setCode(ResponseStatus.failure);
    		output.setDesc("Server fails");
	    }else{
	    	if(!theString.contains("token")){
	    		output.setCode(ResponseStatus.failure);
	    		output.setDesc("Unauthorized");
	    	}else{
	    		output.setCode(ResponseStatus.success);
	    		output.setDesc("Get author token ");
	    		output.setObj(theString);
	    	}
	    }
	    return output;
	}
	
	
	// get kf communites
	@CrossOrigin
	@RequestMapping(value = "/getkfCommunities", method = RequestMethod.POST)
	public CustomResponse getkfCommunities(
			@RequestParam(value="token", required=false) String  token,
			@RequestParam(value="kfurl", required=false) String kfurl
			){
		
		CustomResponse output = new CustomResponse();
		Client client = getClient();
		if(!kfurl.endsWith("/")){
			kfurl=kfurl+"/";
		}
		
	    WebResource webResource = client.resource(kfurl+"api/users/myRegistrations");
	    
	    ClientResponse response = webResource.header(HttpHeaders.AUTHORIZATION, "bearer "+token).type("application/x-www-form-urlencoded").get(ClientResponse.class); 
	    // System.out.println(response);
	    InputStream in =response.getEntityInputStream();
	    StringWriter writer = new StringWriter();
	    try {
			IOUtils.copy(in, writer, "utf-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	    String theString = writer.toString();
	    System.out.println(theString);
	    if(response.getStatus()!=200){
	    	output.setCode(ResponseStatus.failure);
    		output.setDesc("Server fails");
	    }else{
	    	if(theString.equals("Unauthorized")){
	    		output.setCode(ResponseStatus.failure);
	    		output.setDesc("Unauthorized");
	    	}else{
	    		output.setCode(ResponseStatus.success);
	    		output.setDesc("Get communities by author token ");
	    		output.setObj(theString);
	    	}
	    }
	    return output;
}


	
	
	

	private String getTokenFromJson(String json){
		return json.split(":")[1].substring(1,json.split(":")[1].length()-2);
	}
	
	
private Client getClient(){
	TrustManager[] trustAllCerts = new TrustManager[] {
		       new X509TrustManager() {
		          public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		            return null;
		          }

		          public void checkClientTrusted(X509Certificate[] certs, String authType) {  }

		          public void checkServerTrusted(X509Certificate[] certs, String authType) {  }

		       }
		    };
	   HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
	   DefaultClientConfig config = new DefaultClientConfig();
	   config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
	   SSLContext ctx;
	try {
		ctx = SSLContext.getInstance("SSL");
		   ctx.init(null, trustAllCerts, null);
		   config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, ctx));
	
	} catch (NoSuchAlgorithmException | KeyManagementException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   Client client = Client.create(config);
	   return client;
}

public List<Community> getKfCommunitiesFromJson(String json){
	
List<Community> communities = new ArrayList<Community>();
	JSONParser parser = new JSONParser();
	 Object obj;
	try {
		obj = parser.parse(json);
		JSONArray array = (JSONArray)obj;
	    for( Object o : array){
	    	JSONObject jsonObj = (JSONObject)o;
	    	Community community = new Community();
	    	community.setCommunity_id(((String)jsonObj.get("communityId")).toString());
	    	community.setCommunity(((JSONObject)jsonObj.get("_community")).get("title").toString());
	    	community.setRole(((String)jsonObj.get("role")).toString());
	    	community.setEmail(((String)jsonObj.get("email")).toString());
	    	community.setFirstName(((String)jsonObj.get("firstName")).toString());
	    	community.setLastName(((String)jsonObj.get("lastName")).toString());
	    	community.setUserName(((String)jsonObj.get("userName")).toString());
	    	community.setStr_id(((String)jsonObj.get("_id")).toString());
	    	community.setITM(false);
	    	communities.add(community);
	    }
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return communities;
	
}

public Set<String> getKfCommunitiesNameFromJson(String json){
	
Set<String> communities = new HashSet<String>();
	JSONParser parser = new JSONParser();
	 Object obj;
	try {
		obj = parser.parse(json);
		JSONArray array = (JSONArray)obj;
	    for( Object o : array){
	    	JSONObject jsonObj = (JSONObject)o;
	    	communities.add(((JSONObject)jsonObj.get("_community")).get("title").toString());
	    	
	    }
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return communities;
	
}


public String getKfUserIdFromJson(String json){
String uid = "";
	JSONParser parser = new JSONParser();
	 Object obj;
	try {
		obj = parser.parse(json);
		JSONArray array = (JSONArray)obj;
	    for( Object o : array){
	    	JSONObject jsonObj = (JSONObject)o;
	    	uid=jsonObj.get("userId").toString();
	    	break;
	    }
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return uid;
	
}




	

	
}

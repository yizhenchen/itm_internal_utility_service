package edu.albany.util.mysql.utility;

public class CustomerResponse implements java.io.Serializable{

	private static final long serialVersionUID = -7576044225250723284L;
	
	private int code;
	private Object obj;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	
	
	

}

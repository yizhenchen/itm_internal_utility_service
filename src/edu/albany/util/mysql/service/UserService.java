package edu.albany.util.mysql.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.albany.util.mysql.utility.DatabaseUtils;

@Service("userService")
public class UserService {
	final static Logger logger = Logger.getLogger(SqlService.class);
	@Autowired
	public DatabaseUtils db;

	
	public boolean CheckToken(String token) {
		String sql = "SELECT token FROM user WHERE token = ?";
		 List<String> rtoken =  db.getJdbcTemplate().queryForList(sql, new Object[] { token }, String.class);
		if ( rtoken.isEmpty() )
			  return false;
		return true;
	}

	
	public boolean checkUserByUserNamePassword(String userName,String password) {
		String sql = "SELECT token FROM user WHERE username = ? and password=?";
		 List<String> rtoken =  db.getJdbcTemplate().queryForList(sql, new Object[] {userName,password }, String.class);
		if ( rtoken.isEmpty() )
			  return false;
		return true;
	}
	
	
	public boolean CheckPermission(int serviceId, String token) {
		String sql = "SELECT type FROM user WHERE token = ?";

		Integer typeId = (Integer) db.getJdbcTemplate().queryForObject(sql, new Object[] { token }, Integer.class);

		String sql2 = "SELECT service_id FROM services_permission WHERE type_id ="+typeId;

		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql2);
		boolean isvalid = false;
		for (Map row : rows) {
			if ((Integer) row.get("service_id") == serviceId) {
				isvalid = true;
				break;
			};
		}
		return isvalid;
	}
	
	
	public String getToken(int uid){
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
		Random rand = new Random();
		int  n = rand.nextInt(1000) + 1;
		String token = timeStamp+String.valueOf(uid)+String.valueOf(n);
		return token;
		
	}
	
	public void setUserToken() {
		
		String sql = "SELECT id FROM user ";
		List<Integer> ids = db.getJdbcTemplate().queryForList(sql, new Object[] {}, Integer.class);
		System.out.print("Size of User = "+ids.size());
		String[] querys= new String[ids.size()];
		int i = 0;
		for(Integer id :ids){
			i++;
			querys[i]="update user set token ='"+getToken(id)+"' where id ="+id;
		}
		int[] result=db.getJdbcTemplate().batchUpdate(querys);
		System.out.print("Effect of User = "+result.length);

	}
	
	
}

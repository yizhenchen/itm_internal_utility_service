package edu.albany.util.mysql.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.albany.util.mysql.dao.Thread;
import edu.albany.util.mysql.dao.User;
import edu.albany.util.mysql.dao.View;
import edu.albany.util.mysql.dao.requestRegisterCommunity;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import edu.albany.util.mysql.dao.Community;
import edu.albany.util.mysql.dao.Note;
import edu.albany.util.mysql.dao.Project;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;

@Service("ITM3Service")
public class ITM3Operation {

	final static Logger logger = Logger.getLogger(ITM3Operation.class);
	@Autowired
	public DatabaseUtils db;
	
	@Resource
	private SqlService Service;
		
	
	public boolean checkNoteId(String note_id){
		
		boolean flag = false;
		String sql="select * from note_table where"
				+ "  note_id='"+note_id+"' ";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
		for (Map row : rows) {
			flag=true;
		}
		return flag;
	}
	
	
	public void updateNote(Note note){
		
		db.getJdbcTemplate().update(
				"update note_table set title= ?, content=?,modify_time=? where note_id='"+note.getNote_id()+"'",
				note.getTitle(),
				note.getContent(),
				note.getModified_time()
				);
		
	}
	
	

	public void deleteNoteAuthor(Note note){
		
		db.getJdbcTemplate().update(
				"delete from  author_note where note_id=?",note.getNote_id()
				);
		
	}
	
	public List<Project> getProjectsByUserId(String user_id){
		String sql="select project_thread.project_id,project_thread.thread_id,project.title,thread_table.threadfocus "
				+ "from project_thread,project,thread_table,project_member "
				+ "where "
				+ "project_thread.project_id=project.id "
				+ "and project_thread.thread_id = thread_table.id "
				+ "and project.id=project_member.project_id "
				+ "and project_member.author_id='"+user_id+"' "
				+ "group by project_thread.thread_id "
				+ "order by project_thread.project_id asc";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
		List<Project> pids = new ArrayList<Project>();
		int preProject = -1;
		Project p = new Project();
		for (Map row : rows) {
			if(preProject==-1){
				preProject =(Integer)row.get("project_id");
				p.setId((Integer)row.get("project_id"));
				p.setName((String)row.get("title"));
			}
			if((Integer)row.get("project_id")==preProject){
				Thread t = new Thread();
				t.setId((Integer)row.get("thread_id"));
				t.setName((String)row.get("threadfocus"));
				p.getThreads().add(t);
			}else{
				pids.add(p);
				p=new Project();
				p.setId((Integer)row.get("project_id"));
				p.setName((String)row.get("title"));
				Thread t = new Thread();
				t.setId((Integer)row.get("thread_id"));
				t.setName((String)row.get("threadfocus"));
				p.getThreads().add(t);
			}
		}
		if(p!=null){
			
			pids.add(p);
			
		}
		return pids;
	}
	
	public String getLocalDb(String community_id){
		String sql="select localdb from itm3.connection_table where community_id='"+community_id+"'";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
		if (!rows.isEmpty()){
			return (String) rows.get(0).get("localdb");
		}else{
			return null;
		}
	}
	
	
	
	public String updateDatebase(String localdb){
		return null;
	
	}
	
	// get view related to thread
	public List<View> getViewsByThreadId(String thread_id){
		String sql="select note_id from thread_note where thread_id="+thread_id;
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
		List<Note> notes = new ArrayList<Note>();
		List<View> views = new ArrayList<View>();
		for (Map row : rows) {
			Note note = new Note();
			String sql2="select view_note.view_id,title from view_note,view_table where view_note.note_id='"+(String) row.get("note_id")+"' and view_table.view_id=view_note.view_id";
			
			List<Map<String, Object>> rows2 = db.getJdbcTemplate().queryForList(sql2);
			
			for(Map row2: rows2){
				boolean isAlreadyExist =false;
				for(View v : views){
					if(v.getId().equals((String) row2.get("view_id"))){
						isAlreadyExist = true;
					}
				}
				if(isAlreadyExist){
					// do nothing  
				}else{
					View v = new View();
					v.setId((String) row2.get("view_id"));
					v.setName((String) row2.get("title"));
					views.add(v);
				}
			}
		}
		
		return views;
	}

	
	// get notes and user info belong to the view by view id
	// return value [note_id,[author_ids]]
	public List<Note> getNotesInfoByViewIdAndAuthorID(String view_id,String author_id){
		String sql="select note_id from view_note where view_id='"+view_id+"'";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
		List<Note> notes = new ArrayList<Note>();
		for (Map row : rows) {
			Note note = new Note();
			note.setNote_id((String) row.get("note_id"));
			
			// get author_id
			String sql2="select author_id from author_note where note_id='"+(String) row.get("note_id")+"'";
			List<Map<String, Object>> rows2 = db.getJdbcTemplate().queryForList(sql2);
			List<String> author_ids = new ArrayList<String>();
			boolean isBelongsToAuthor = false;
			for(Map row2: rows2){
				if(((String) row2.get("author_id")).equals(author_id)){
					isBelongsToAuthor=true;
				}
				author_ids.add((String) row2.get("author_id"));
			}
			note.setAuthor_id(author_ids);
			if(isBelongsToAuthor){
				notes.add(note);
			}
		}
		return notes;
	}
			
	
	
				
	// find project and thread info by notes 
	public List<Project> getProjectByNotes(List<String> note_ids){
		Map<Integer,Project> projects =  new HashMap<Integer,Project>();
		for(String id : note_ids){
			String sql="select project_id,thread_id from thread_note where note_id='"+id+"'";
			List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
			for(Map row: rows){
				 if(projects.containsKey((Integer)row.get("project_id"))){
					  Project p = projects.get((Integer)row.get("project_id"));
					  List<Thread> threads = p.getThreads();
					  boolean isThreadExist=false;
					  for(Thread t : threads){
						  if (t.getId()== (Integer)row.get("thread_id")){
							   isThreadExist = true;
						  }
					  }
					  if(!isThreadExist){
						  Thread t = new Thread();
						  t.setId((Integer)row.get("thread_id"));
						  projects.get((Integer)row.get("project_id")).getThreads().add(t);
					  }
				 }else{
					 Project p = new Project();
					 p.setId((Integer)row.get("project_id"));
					 Thread t = new Thread();
					 t.setId((Integer)row.get("thread_id"));
					 p.getThreads().add(t);
					 projects.put((Integer)row.get("project_id"), p);
				 }
			}
						
		}
		// getting all the project and thread id, then to get the project and thread name
		
				// get all project and its ids
				String sql2="select id,title from project";
				List<Project> projectAll = new ArrayList<Project>();
				List<Map<String, Object>> rows2 = db.getJdbcTemplate().queryForList(sql2);
				for(Map row: rows2){
					Project p = new Project();
					p.setId((Integer)row.get("id"));
					p.setName((String)row.get("title"));
					projectAll.add(p);
				}
				// get all thread and its ids
				String sql3="select id,threadfocus from thread_table";
				List<Thread> threadAll = new ArrayList<Thread>();
				List<Map<String, Object>> rows3 = db.getJdbcTemplate().queryForList(sql3);
				for(Map row: rows3){
					Thread t = new Thread();
					t.setId((Integer)row.get("id"));
					t.setName((String)row.get("threadfocus"));
					threadAll.add(t);
				}
		
		for(Integer pid : projects.keySet()){
			for(Project p : projectAll){
				if(pid.equals(p.getId())){
					projects.get(pid).setName(p.getName());
				}
			}
			
			for(int i =0; i<projects.get(pid).getThreads().size();i++){
				Integer tid =projects.get(pid).getThreads().get(i).getId();
				for(Thread t:threadAll){
					if(tid.equals(t.getId())){
						projects.get(pid).getThreads().get(i).setName(t.getName());
					}
				}
			}
		}
		
		List<Project> project_list=new ArrayList<Project>();
		for(Integer pid : projects.keySet()){
			project_list.add(projects.get(pid));
		}
		return project_list;
	}
	
	
	// add note into note table 
	
	public boolean addNoteIntoNoteTable(Note note){
		db.getJdbcTemplate().update(
				"insert ignore into note_table (note_id,title,content,create_time,modify_time,status)values(?,?,?,?,?,?)",
				note.getNote_id(),
				note.getTitle(),
				note.getContent(),
				note.getCreate_time(),
				note.getModified_time(),
				"active"
				);
		
		return true;
	}
	
	// add note thread information
	public boolean addNoteIntoThreadTable(int tid,String nid){
		String sql="select project_id from project_thread where thread_id='"+tid+"'";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
		int pid =-1;
		if (!rows.isEmpty()){
			pid=(Integer) rows.get(0).get("project_id");
		}else{
			return false;
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		db.getJdbcTemplate().update(
				"insert ignore into thread_note (project_id,thread_id,note_id,create_time)values(?,?,?,?)",
				pid,
				tid,
				nid,
				dateFormat.format(date)
				);
		
		
		return true;
	}
	
	// add note author information
	public boolean addNoteAuthorInfo(String author_id,String note_id){
		db.getJdbcTemplate().update(
				"insert ignore into author_note (author_id,note_id)values(?,?)",
				author_id,
				note_id
				);
		
		return true;
	}
	
	public void createDatabase(String database_name,String configureFiel){
		
		
	}
	
	
	
	// get user by string id
	public boolean getUserByStrId(String author_id){
		String sql="select * from user where str_id='"+author_id+"'";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
		if (!rows.isEmpty()){
			return true;
		}else{
			return false;
		}
	}
		
		

	
		public void  addUserInfoInToCommunity(User u){
				db.getJdbcTemplate().update(
						"Insert Ignore into user (first_name,last_name,username,type,email,str_id,token,community) values(?,?,?,?,?,?,?,?)",
						u.getFirstName(),
						u.getLastName(),
						u.getUserName(),
						u.getType(),
						u.getEmail(),
						u.getStr_id(),
						u.getToken(),
						u.getCommunity()
						);
			}

		
		
		public void addUserInfoIntoITM(User u){
			db.getJdbcTemplate().update(
					"Insert Ignore into user (first_name,last_name,username,type,email,str_id,token,community) values(?,?,?,?,?,?,?,?)",
					u.getFirstName(),
					u.getLastName(),
					u.getUserName(),
					u.getType(),
					u.getEmail(),
					u.getStr_id(),
					u.getToken(),
					u.getCommunity_name()
					);
			}

		
		

		public void addUserCommunityInfoIntoITM(User u){
		       SimpleDateFormat ft =  new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
		       String date = ft.format(new Date());
		       db.getJdbcTemplate().update(
					"Insert Ignore into user_community (user_id,community,type,localdb,token,username,community_id,last_login_time) values(?,?,?,?,?,?,?,?)",
					u.getStr_id(),
					u.getCommunity_name(),
					u.getType(),
					u.getLocalDatabases(),
					u.getToken(),
					u.getUserName(),
					u.getCommunity(),
					date
					);
			}

		
		
		
	public void addCommunity(requestRegisterCommunity community){
		
		db.getJdbcTemplate().update(
				"insert into itm3.connection_table (community_name,localdb,community_id,access_code,write_code,manager_code,status)values(?,?,?,?,?,?,?)",
				community.getCommunity_name(),
				StringUtils.join(community.getCommunity_name().split(" "), "_"),
				community.getCommunity_id(),
				"",
				community.getWriter_code(),
				community.getManager_code(),
				"active"
				);
		
	}
	public boolean checkUser(String token,String kfurl,String community_id){
		Client client = getClient();
		if(!kfurl.endsWith("/")){
			kfurl=kfurl+"/";
		}
		WebResource webResource = client.resource(kfurl+"/api/authors/"+community_id+"/me");
	    
	    MultivaluedMap formData = new MultivaluedMapImpl();
	    formData.add("token_string", token);
	    ClientResponse response = webResource.accept("application/json").type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
	    InputStream in =response.getEntityInputStream();
	    StringWriter writer = new StringWriter();
	    
	    try {
			IOUtils.copy(in, writer, "utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    String theString = writer.toString();
	    System.out.println(theString);
	    if(response.getStatus()!=200){
	    	return false;
	    }else{
	    	if(!theString.contains("token")){
	    		return false;
	    	}else{
	    		return true;
	    	}
	    }
	}

	public User getUser(String kfurl ,String token ,String community_id) throws  org.json.simple.parser.ParseException{
		Client client = getClient();
		if(!kfurl.endsWith("/")){
			kfurl=kfurl+"/";
		}
		WebResource webResource = client.resource(kfurl+"api/authors/"+community_id+"/me");
	    ClientResponse response = webResource.header(HttpHeaders.AUTHORIZATION, "bearer "+token).type("application/x-www-form-urlencoded").get(ClientResponse.class); 
	    // System.out.println(response);
	    InputStream in =response.getEntityInputStream();
	    StringWriter writer = new StringWriter();
	    try {
			IOUtils.copy(in, writer, "utf-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	    String jsonStr = writer.toString();

	    if(response.getStatus()!=200){
	    	return null;
	    }else{
	    	if(jsonStr.contains("userName")){
	    		JSONParser parser = new JSONParser();
	 		    Object obj = parser.parse(jsonStr);
	 	        JSONObject jsonObject = (JSONObject) obj;
	 	        User user = new User();
	 	        user.setAccess_token(token);
	 	        user.setEmail((String) jsonObject.get("email"));
	 	        user.setStr_id((String) jsonObject.get("_id"));
	 	        user.setFirstName((String) jsonObject.get("firstName"));
	 	        user.setLastName((String) jsonObject.get("lastName"));
	 	        user.setUserName((String) jsonObject.get("userName"));
	 	        user.setType((String) jsonObject.get("role"));
	 	       JSONObject communityJson= (JSONObject) jsonObject.get("_community");
	 	        user.setCommunity(community_id);
	 	        user.setCommunity_name((String)communityJson.get("title") );
	 	        return user;
	    	}
	    	return null;
	    }
	}
	
	
	private Client getClient(){
		TrustManager[] trustAllCerts = new TrustManager[] {
			       new X509TrustManager() {
			          public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			            return null;
			          }

			          public void checkClientTrusted(X509Certificate[] certs, String authType) {  }

			          public void checkServerTrusted(X509Certificate[] certs, String authType) {  }

			       }
			    };
		   HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
		   DefaultClientConfig config = new DefaultClientConfig();
		   config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		   SSLContext ctx;
		try {
			ctx = SSLContext.getInstance("SSL");
			   ctx.init(null, trustAllCerts, null);
			   config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, ctx));
		
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   Client client = Client.create(config);
		   return client;
	}
	
	
	
	public User transfortToITMUser(User kfuser) {
		

		if(kfuser.getType().trim().toLowerCase().equals("manager")){
			kfuser.setType("3");
		}
		if(kfuser.getType().trim().toLowerCase().equals("writer")){
			kfuser.setType("2");
		}
		if(kfuser.getType().trim().toLowerCase().equals("editor")){
			kfuser.setType("2");
		}
		if(kfuser.getType().trim().toLowerCase().equals("reader")){
			kfuser.setType("0");
		}
		return kfuser;
	}
			
	public String getToken(int uid){
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
		Random rand = new Random();
		int  n = rand.nextInt(1000) + 1;
		String token = timeStamp+String.valueOf(uid)+String.valueOf(n);
		return token;
		
	}


	public String getUserTokenByStrId(String str_id) {
		// TODO Auto-generated method stub
		String sql="select token from user where str_id='"+str_id+"'";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(sql);
		if (!rows.isEmpty()){
			return (String) rows.get(0).get("token");
		}else{
			return null;
		}
		
	}
		
	public static void main(String arg[]){
		
		ITM3Operation op = new ITM3Operation();
				op.createDatabase("localdb",null);
		
	}


	public void importKFdata(String server, String token, String dbName) throws IOException {

		// set up the command and parameter
		String pythonScriptPath = "E:\\ITM\\kf6-itm3-token.py -t "+token+" -s "
								+server+" -S localhost -U root -P "+db.getPassword()
								+"-D "+dbName;
		String[] cmd = new String[2];
		cmd[0] = "python"; // check version of installed python: python -V
		cmd[1] = pythonScriptPath;
		 
		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec(cmd);
		 
		// retrieve output from python script
		BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while((line = bfr.readLine()) != null) {
		// display each output line form python script
			System.out.println(line);
			}
		}
		
	
}

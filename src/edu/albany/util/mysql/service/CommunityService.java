package edu.albany.util.mysql.service;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.albany.util.mysql.dao.Community;
import edu.albany.util.mysql.dao.KF6Database;
import edu.albany.util.mysql.dao.LocalDB;
import edu.albany.util.mysql.utility.DatabaseUtils;



@Service("communityService")
public class CommunityService {
	
	
	final static Logger logger = Logger.getLogger(SqlService.class);
	@Autowired
	public DatabaseUtils db;

	
	public List<Community> getLocalComunities(){
		List<Community> communities = new ArrayList<Community>();
		String selectQuery="select * from connection_table where status ='active' ";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(selectQuery);
		for (Map row : rows) {
			Community c = new Community();
			c.setCommunity((String) row.get("community_name"));
			c.setLocaldb((String) row.get("localdb"));
			c.setCommunity_id((String) row.get("community_id"));
			c.setITM(false);
			communities.add(c);
		}
		return communities;
	}
	
	public List<Community> getLocalComunities(String email, String pwd){
		List<Community> communities = new ArrayList<Community>();
			String selectQuery="select * from itm3.user,itm3.user_community  where  user_community.user_id=itm3.user.str_id and email='"+email+"' and password='"+pwd+"'  ";
			List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(selectQuery);
		for (Map row : rows) {
			Community c = new Community();
			c.setCommunity_id((String) row.get("community_id"));
			c.setCommunity((String) row.get("community"));
			c.setLocaldb((String) row.get("localdb"));
			c.setRole((String) row.get("type"));
			c.setEmail((String) row.get("email"));
			c.setFirstName((String) row.get("first_name"));
			c.setLastName((String) row.get("last_name"));
			c.setStr_id((String) row.get("str_id"));
			c.setToken((String) row.get("token"));
			c.setITM(true);
			communities.add(c);
		}
		return communities;
	}
	
	
	
	public List<Community> getLocalComunities(String str_id){
		List<Community> communities = new ArrayList<Community>();
		String selectQuery="select * from itm3.user ,itm3.user_community  where   user_community.user_id=itm3.user.str_id and str_id='"+str_id+"'  ";
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(selectQuery);
		for (Map row : rows) {
			Community c = new Community();
			c.setCommunity_id((String) row.get("community_id"));
			c.setCommunity((String) row.get("community"));
			c.setLocaldb((String) row.get("localdb"));
			c.setRole((String) row.get("type"));
			c.setEmail((String) row.get("email"));
			c.setFirstName((String) row.get("first_name"));
			c.setLastName((String) row.get("last_name"));
			c.setStr_id((String) row.get("str_id"));
			c.setToken((String) row.get("token"));
			c.setITM(true);
			communities.add(c);
		}
		return communities;
	}
	
	




	
}

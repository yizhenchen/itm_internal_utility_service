package edu.albany.util.mysql.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.Statement;

import edu.albany.util.mysql.utility.DatabaseUtils;

@Service("Service")
public class SqlService {
	final static Logger logger = Logger.getLogger(SqlService.class);
	@Autowired
	public DatabaseUtils db;
		

	public String get(String query){
		System.out.println("[GET] Query : " +query);
		
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(query);
		String log = "insert into  (";
		if(!rows.isEmpty()){
			try {
				return convertTOJson(rows);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	public List<Map<String, Object>> getWithOutJson(String query){
		System.out.println("[GET] Query : " +query);
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(query);
		return rows;
	}
	public Number update( String query){
		final String q2 =query;
		if(!query.toLowerCase().startsWith("insert")){
			System.out.println("[POST] Query : " +query);
			return db.getJdbcTemplate().update(query);
		}
		else{
			GeneratedKeyHolder holder = new GeneratedKeyHolder();
			db.getJdbcTemplate().update(
					new PreparedStatementCreator() {
				        public PreparedStatement createPreparedStatement(Connection con)
				                throws SQLException {
				        	 return con.prepareStatement(q2, Statement.RETURN_GENERATED_KEYS);
				        }
				    }, holder
				);
			return holder.getKey();
		}
	}

	public String convertTOJson(Object obj) throws JsonGenerationException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(obj);
	}
	
	
}

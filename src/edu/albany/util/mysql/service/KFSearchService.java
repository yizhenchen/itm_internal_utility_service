package edu.albany.util.mysql.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.albany.util.mysql.utility.DatabaseUtils;

@Service("searchService")
public class KFSearchService {

	final static Logger logger = Logger.getLogger(KFSearchService.class);
	@Autowired
	public DatabaseUtils db;
	
	@Resource
	public SqlService Service;
		
		
	
	public String search(
			String view_ids, String project_ids, String from_date, String to_date, 
			String isExact, String category, String keywords, String users )
	{
		String output = "" ;
		
		String vids = view_ids;
		if(vids.length()>2){
			vids=vids.substring(1, vids.length()-1);
		}else{
			vids="";
		}
		
		
		String pids=project_ids.replaceAll("[^0-9]+", ",");
		if(pids.length()>2){
			pids=pids.substring(1, pids.length()-1);
		}else{
			pids="";
		}
		
		String users_ids ="";
		if(users.length()>3){
			users_ids= users.substring(1,users.length()-1);
		}else{
			users_ids="";
		}
		
		// conditions
		String fuzzyTitleContent = "";
		String fuzzyTitle = "";
		String fuzzyContent = "";
		String matchTitleContent = "";
		String matchTitle ="";
		String matchContent = "";
		String test="";
		String[] arr_keywords = keywords.split(",");
		for (int i = 0 ; i <arr_keywords.length ; i++ ) {
		     // arr_keywords[i]; 
			fuzzyTitleContent=fuzzyTitleContent+"or title LIKE '%"+arr_keywords[i]+"%' or content LIKE '%"+arr_keywords[i]+"%'  ";
			fuzzyTitle=fuzzyTitle+"or title LIKE '%"+arr_keywords[i]+"%'  ";
			fuzzyContent=fuzzyContent+"or  content LIKE '%"+arr_keywords[i]+"%'  ";
			matchTitleContent=matchTitleContent+"or title ='"+arr_keywords[i]+"' or content ='"+arr_keywords[i]+"'  ";
			matchTitle=matchTitle+"or title ='"+arr_keywords[i]+"'  ";
			matchContent=matchContent+"or  content ='"+arr_keywords[i]+"'  ";
		 } 
		fuzzyTitleContent = fuzzyTitleContent.substring(2,fuzzyTitleContent.length());
	    fuzzyTitle = fuzzyTitle.substring(2,fuzzyTitle.length());
		fuzzyContent =  fuzzyContent.substring(2,fuzzyContent.length());
		matchTitleContent = matchTitleContent.substring(2,matchTitleContent.length());
		matchTitle =matchTitle.substring(2,matchTitle.length());
		matchContent = matchContent.substring(2,matchContent.length());
		String baseQuery ="select * from note_table";

	    boolean has_where = false;
		
		if((null!=vids&&!vids.isEmpty())&&(null==pids||pids.isEmpty())){
			baseQuery="select * from (select * from note_table where note_id in (select note_id from view_note where view_id in ("+vids+"))) as a   ";
		}

		if((null!=pids&&!pids.isEmpty())&&(null!=vids&&!vids.isEmpty())){
			 baseQuery="select * from ( select * from note_table where note_id in (select note_id from view_note where view_id in ("+vids+"))"
			 +"or note_id in (select note_id from thread_note where project_id in ("+pids+"))) as a   ";
		}
		
		if((null!=pids&&!pids.isEmpty())&&(null==vids||vids.isEmpty())){
			 baseQuery="select * from ( select * from note_table where note_id in (select note_id from thread_note where project_id in ("+pids+"))) as a   ";
		}
		
		if(users_ids!=""&&null!=users_ids){
			if(baseQuery.length()>25){
				baseQuery= baseQuery.substring(0, baseQuery.length()-9) + "   and note_id in (select note_id from author_note where author_note.author_id in ("+users_ids+"))) as a   ";
			}else{
				has_where=true;
				baseQuery= baseQuery + "  where note_id in (select note_id from author_note where author_note.author_id in ("+users_ids+"))";
			}
		}
		
		if(!keywords.equals("")&&!keywords.equals(null)){
			has_where=true;
			
			if(isExact.equals("1")){//Exact match search
				if(category.equals("0")){// find by title and content 
					if(null!=vids&&!vids.isEmpty()||null!=pids&&!pids.isEmpty()){
						baseQuery=baseQuery+" where "+matchTitleContent;
					}else{
						if(users_ids!=""&&null!=users_ids){
							baseQuery=baseQuery+" and "+matchTitleContent;
						}else{
						
							baseQuery=baseQuery+" where "+matchTitleContent;
						}
					}
					
				}
				if(category.equals("2")){ // find by title only
					if(null!=vids&&!vids.isEmpty()||null!=pids&&!pids.isEmpty()){
						baseQuery=baseQuery+" where  "+matchTitle;
					}else{
						if(users_ids!=""&&null!=users_ids){
							baseQuery=baseQuery+" and "+matchTitle;
						}else{
							
							baseQuery=baseQuery+" where  "+matchTitle;
						}
					}
				
				}
				if(category.equals("1")){
					// find by content only
					if(null!=vids&&!vids.isEmpty()||null!=pids&&!pids.isEmpty()){
						baseQuery=baseQuery+" where "+matchContent;
					}else{
						if(users_ids!=""&&null!=users_ids){
							baseQuery=baseQuery+" and "+matchContent;
						}else{
							baseQuery=baseQuery+" where "+matchContent;
						}
					}
					
					
				}
			}else{
				// fuzzy match search
				if(category.equals("0")){// find by title and content 
					if(null!=vids&&!vids.isEmpty()||null!=pids&&!pids.isEmpty()){
						baseQuery=baseQuery+" where  "+fuzzyTitleContent;
					}else{
						if(users_ids!=""&&null!=users_ids){
							baseQuery=baseQuery+" and "+fuzzyTitleContent;
						}else{
							baseQuery=baseQuery+" where "+fuzzyTitleContent;
						}
					}
				}
				if(category.equals("2")){ // find by title only
					if(null!=vids&&!vids.isEmpty()||null!=pids&&!pids.isEmpty()){
						baseQuery=baseQuery+"  "+fuzzyTitle;
					}else{
						if(users_ids!=""&&null!=users_ids){
							baseQuery=baseQuery+" and "+fuzzyTitle;
						}else{
							baseQuery=baseQuery+" where "+fuzzyTitle;
						}
					
					}
					
				}
				if(category.equals("1")){// find by content only
					if(null!=vids&&!vids.isEmpty()||null!=pids&&!pids.isEmpty()){
						baseQuery=baseQuery+"  "+fuzzyContent;
					}else{
						if(users_ids!=""&&null!=users_ids){
							baseQuery=baseQuery+" and "+fuzzyContent;
						}else{
							baseQuery=baseQuery+" where "+fuzzyContent;
						}
					}
				}
			}
			
		}
		
		
		// time
		if(!from_date.isEmpty()&&!to_date.isEmpty()){
			try {
				SimpleDateFormat dateParser = new SimpleDateFormat("dd MMMMM, yyyy");
				Date date;
				date = dateParser.parse(to_date);
				SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				to_date=dateFormatter.format(date);
				Date date2 = dateParser.parse(from_date);
				from_date=dateFormatter.format(date2);
				if(has_where){
					baseQuery=baseQuery+" and  create_time > '"+from_date+"'  and create_time< '"+to_date+"'";
				}
				
				if(!has_where){
					baseQuery=baseQuery+" where create_time > '"+from_date+"'  and create_time< '"+to_date+"'";
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		System.out.println(baseQuery);
		List<Map<String, Object>> res3 = Service.getWithOutJson(baseQuery);
		
		

		// get note_id and author_name
		String noteAuthorQuery ="select GROUP_CONCAT(DISTINCT(first_name)) as first_name , last_name ,note_id from user,author_note where author_note.author_id=user.str_id group by author_note.note_id";
		List<Map<String, Object>> res2 = Service.getWithOutJson(noteAuthorQuery);
		Map<String,String> authorNoteIds= new HashMap<String,String>();
		for(Map<String,Object> id :res2){
			authorNoteIds.put((String) id.get("note_id"), (String) id.get("first_name")+" "+(String) id.get("last_name"));
		}
		
		
		
		for(Map<String,Object> id :res3){
			id.put("first_name", authorNoteIds.get(id.get("note_id")));
		}
		
		try {
			output=Service.convertTOJson(res3);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return output; 
		
	}
}

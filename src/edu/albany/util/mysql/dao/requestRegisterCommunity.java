package edu.albany.util.mysql.dao;

public class requestRegisterCommunity {

	private String community_name;
	private String manager_code;
	private String writer_code;
	private String reader_code;
	private String community_id;
	private String community_url;
	
	private String account;
	private String password;
	
	private String token;
	
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCommunity_name() {
		return community_name;
	}
	public void setCommunity_name(String community_name) {
		this.community_name = community_name;
	}
	public String getManager_code() {
		return manager_code;
	}
	public void setManager_code(String manager_code) {
		this.manager_code = manager_code;
	}
	public String getWriter_code() {
		return writer_code;
	}
	public void setWriter_code(String writer_code) {
		this.writer_code = writer_code;
	}
	public String getReader_code() {
		return reader_code;
	}
	public void setReader_code(String reader_code) {
		this.reader_code = reader_code;
	}
	public String getCommunity_id() {
		return community_id;
	}
	public void setCommunity_id(String community_id) {
		this.community_id = community_id;
	}
	public String getCommunity_url() {
		return community_url;
	}
	public void setCommunity_url(String community_url) {
		this.community_url = community_url;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}

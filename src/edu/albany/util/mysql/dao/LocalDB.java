package edu.albany.util.mysql.dao;

public class LocalDB {
	private String remote_data_source_name;
	private String remote_data_source;
	private String local_database;
	
	public String getRemote_data_source_name() {
		return remote_data_source_name;
	}
	public void setRemote_data_source_name(String remote_data_source_name) {
		this.remote_data_source_name = remote_data_source_name;
	}
	public String getRemote_data_source() {
		return remote_data_source;
	}
	public void setRemote_data_source(String remote_data_source) {
		this.remote_data_source = remote_data_source;
	}
	public String getLocal_database() {
		return local_database;
	}
	public void setLocal_database(String local_database) {
		this.local_database = local_database;
	}
	

}

package edu.albany.util.mysql.dao;

public class RequestToGetThreadView {
	private String community_id;
	private String thread_id;
	public String getCommunity_id() {
		return community_id;
	}
	public void setCommunity_id(String community_id) {
		this.community_id = community_id;
	}
	public String getThread_id() {
		return thread_id;
	}
	public void setThread_id(String thread_id) {
		this.thread_id = thread_id;
	}
	
	

}

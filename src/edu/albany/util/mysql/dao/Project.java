package edu.albany.util.mysql.dao;

import java.util.ArrayList;
import java.util.List;

public class Project {
	
	private int id;
	private String name;
	private List<Thread> threads = new ArrayList<Thread>();
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Thread> getThreads() {
		return threads;
	}
	public void setThreads(List<Thread> threads) {
		this.threads = threads;
	}
	
	
	
}

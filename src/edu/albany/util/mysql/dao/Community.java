package edu.albany.util.mysql.dao;

public class Community {
	private String community;
	private String role;
	private String localdb;
	private boolean isITM; 
	private String firstName;
	private String lastName;
	private String email;
	private String userName;
	private String str_id;
	private String token;
	private String community_id;
	
	
	
	
	public String getCommunity_id() {
		return community_id;
	}
	public void setCommunity_id(String community_id) {
		this.community_id = community_id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getStr_id() {
		return str_id;
	}
	public void setStr_id(String str_id) {
		this.str_id = str_id;
	}
	public boolean isITM() {
		return isITM;
	}
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public boolean getisITM() {
		return isITM;
	}
	public void setITM(boolean isITM) {
		this.isITM = isITM;
	}
	
	public String getLocaldb() {
		return localdb;
	}
	public void setLocaldb(String localdb) {
		this.localdb = localdb;
	}

	
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
}

package edu.albany.util.mysql.dao;

public class requestToGetUserToken {

	private String community_url;
	private String token;
	private String community_id;
	
	
	public String getCommunity_url() {
		return community_url;
	}
	public void setCommunity_url(String community_url) {
		this.community_url = community_url;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCommunity_id() {
		return community_id;
	}
	public void setCommunity_id(String community_id) {
		this.community_id = community_id;
	}
	
	
	
}

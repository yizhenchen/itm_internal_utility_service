package edu.albany.util.mysql.dao;

import java.util.List;

public class Note {
	
	private String note_id;
	private String title;
	private String content;
	private String create_time;
	private String modified_time;
	private List<String> author_id;
	
	public List<String> getAuthor_id() {
		return author_id;
	}
	public void setAuthor_id(List<String> author_id) {
		this.author_id = author_id;
	}
	public String getNote_id() {
		return note_id;
	}
	public void setNote_id(String note_id) {
		this.note_id = note_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getModified_time() {
		return modified_time;
	}
	public void setModified_time(String modified_time) {
		this.modified_time = modified_time;
	}
	
	
	

}

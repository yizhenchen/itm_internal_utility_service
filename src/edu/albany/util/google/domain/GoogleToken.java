package edu.albany.util.google.domain;

public class GoogleToken {

	String username;
	String access_token;
	String refresh_token;
	long expire_time;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getRefresh_token() {
		return refresh_token;
	}
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	public long getExpire_time() {
		return expire_time;
	}
	public void setExpire_time(long expire_time) {
		this.expire_time = expire_time;
	}
	
	
}

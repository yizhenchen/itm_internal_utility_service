 package edu.albany.util.google.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.auth.oauth2.Credential;

import edu.albany.util.google.GoogleOAuthClient;
import edu.albany.util.google.service.GoogleService;
import edu.albany.util.mysql.controller.IndexController;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;
import edu.albany.util.mysql.utility.ResponseStatus;
import edu.albany.util.mysql.utility.ResponseUtil;

@RestController
@RequestMapping("/google/")
public class GoogleController {

final static Logger logger = Logger.getLogger(IndexController.class);
	
	@Autowired
	public DatabaseUtils db;
	
	@Autowired
	public ResponseUtil responseUtil;
	
	@Resource
	private GoogleService googleService;
	
	// Generate token
	@CrossOrigin
	@RequestMapping(value = "/oAuth2/getUrl", method = RequestMethod.POST)
	public  CustomResponse getUrl(HttpServletRequest request,  HttpServletResponse response)  {
		String username = (String)request.getParameter("username");
		String url = GoogleOAuthClient.getOAuthUrl(username);
		CustomResponse cr = new CustomResponse();
		cr.setCode(ResponseStatus.success);
		cr.setDesc("Get oauth url.");
		cr.setObj(url);
		return cr;
		
	}
	
	
	// Generate token
	@CrossOrigin
	@RequestMapping(value = "/oAuth2/getToken", method = RequestMethod.GET)
	public void getToken(HttpServletRequest request,  HttpServletResponse response) throws Exception  {
		CustomResponse cr = new CustomResponse();
		cr.setCode(ResponseStatus.success);
		
		if(request.getParameter("code") != null){
			GoogleOAuthClient.getTokenFromGoogle(googleService, request.getParameter("code"),"http://localhost:8080/IIUS/google/oAuth2/getToken", (String)request.getParameter("state"));
			cr.setDesc("permission granted.");
			cr.setObj("http://localhost:8080/ITM3/thread/thread.jsp");
			response.sendRedirect("http://localhost:8080/ITM3/thread/thread.jsp");
		}
		else{
			cr.setDesc("failed to get token from Google, access denied.");
			cr.setObj("");
			String message = "<p>failed to get token from Google, access denied.<p/><a href=\"http://localhost:8080/ITM3/thread/thread.jsp\">Return to thread page</a>";
			response.getWriter().write(message);
		}
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/note/add", method = RequestMethod.POST)
	public  CustomResponse addNote(HttpServletRequest request,  HttpServletResponse response) throws Exception  {
		String pid = request.getParameter("project_id").toString();
	    String tid = request.getParameter("thread_id").toString();
	    String username = request.getParameter("username").toString();
        String viewids = request.getParameter("views").toString();
        String scaffolds = request.getParameter("scaffolds").toString();
        String title = request.getParameter("title").toString();
        String coauthors = request.getParameter("coauthors").toString();
        String plain_content = request.getParameter("plain_content").toString();
        
		Credential crd = GoogleOAuthClient.getCredential(googleService,username);
        String docid = googleService.addNote(crd, pid, tid, username, viewids, title, scaffolds, coauthors,plain_content);

		CustomResponse cr = new CustomResponse();
		cr.setCode(ResponseStatus.success);
		cr.setDesc("Created note.");
		cr.setObj(docid);
		return cr;
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/note/refresh", method = RequestMethod.POST)
	public  CustomResponse refreshNote(HttpServletRequest request,  HttpServletResponse response)  {
		String username = (String)request.getParameter("username");
		String docid = (String)request.getParameter("docid");
		Credential crd = GoogleOAuthClient.getCredential(googleService, username);
        googleService.refreshNote(crd, docid);
		CustomResponse cr = new CustomResponse();
		cr.setCode(ResponseStatus.success);
		cr.setDesc("Note refreshed.");
		cr.setObj("");
		return cr;
		
	}
	
}

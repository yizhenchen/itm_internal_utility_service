package edu.albany.util.google;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.client.util.store.MemoryDataStoreFactory;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.Drive;

import edu.albany.util.google.service.GoogleService;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

public class GoogleOAuthClient {
	
	
	    /** Application name. */
	    private static final String APPLICATION_NAME = "NoteApp";

	    /** Global instance of the {@link FileDataStoreFactory}. */
	    private static MemoryDataStoreFactory TOKEN_STORE_FACTORY;
	    
	    /** Global instance of the GoogleClientSecrets. */
	    private static GoogleClientSecrets CLIENTSECRETS;

	    /** Global instance of the JSON factory. */
	    private static final JsonFactory JSON_FACTORY =
	        JacksonFactory.getDefaultInstance();

	    /** Global instance of the HTTP transport. */
	    private static HttpTransport HTTP_TRANSPORT;

	    /** Global instance of the scopes required by this quickstart.
	     *
	     * If modifying these scopes, delete your previously saved credentials
	     * at ~/.credentials/drive-java-quickstart
	     */
	    private static final List<String> SCOPES =
	        Arrays.asList(DriveScopes.DRIVE);
	    
	    private static GoogleAuthorizationCodeFlow FLOW;
	    
	    static {
	        try {
	            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
	            TOKEN_STORE_FACTORY = MemoryDataStoreFactory.getDefaultInstance();
	            // Load client secrets.
		        InputStream in =
		        		GoogleOAuthClient.class.getResourceAsStream("/client_secret.json");
		        CLIENTSECRETS =
		            GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
		        FLOW = new GoogleAuthorizationCodeFlow.Builder(
		                        HTTP_TRANSPORT, JSON_FACTORY, CLIENTSECRETS, SCOPES)
		                .setDataStoreFactory(TOKEN_STORE_FACTORY)
		                .setAccessType("offline")
		                .setApprovalPrompt("force")
		                .build();
	        } catch (Throwable t) {
	            t.printStackTrace();
	            System.exit(1);
	        }
	    }
	    
	    public static String getOAuthUrl(String userId){
	        // Build flow and trigger user authorization request.
	        return FLOW.newAuthorizationUrl().setState(userId)
	        .setRedirectUri("http://localhost:8080/IIUS/google/oAuth2/getToken").build();
	    }
	    
	    public static Credential getTokenFromGoogle(GoogleService googleService, String code,String dst, String username){
	    	try{
	    		TokenResponse tr = FLOW.newTokenRequest(code).setRedirectUri(dst).execute();
	    		//save token to database
	    		Credential crd = FLOW.createAndStoreCredential(tr, username);
	    		googleService.saveToken(crd, username, tr.getAccessToken(), tr.getRefreshToken(), tr.getExpiresInSeconds());
	    		return crd;
	    	}
	    	catch(Exception e){
		        e.printStackTrace();
	    	}
	    	return null;
	    }
	    
	    /**
	     * create new credential with empty token value
	     * @param userId
	     * @return
	     */
	    public static Credential getNewCredential(String username){
	    	return FLOW.getNewCredential(username);
	    }
	    
	    /**
	     * get credential if exists, or return null
	     * @param userId
	     * @return
	     */
	    public static Credential getCredential(GoogleService googleService, String username){
	    	try{
	    		Credential result = FLOW.loadCredential(username);
	    		if(result == null){
	    			result = googleService.getToken(username);
	    			if(result != null){
	    				FLOW.getCredentialDataStore().set(username, new StoredCredential(result));
	    			}
	    		}
	    		return result;
	    	}
	    	catch(Exception e){
		        e.printStackTrace();
	    	}
	    	return null;
	    }

	    /**
	     * Build and return an authorized Drive client service.
	     * @return an authorized Drive client service
	     * @throws IOException
	     */
	    public static Drive getDriveService(Credential credential){
	    	try{
	    		return new Drive.Builder(
		                HTTP_TRANSPORT, JSON_FACTORY, credential)
		                .setApplicationName(APPLICATION_NAME)
		                .build(); 
	    	}
	    	catch(Exception e){
		        e.printStackTrace();
	    	}
	    	return null;
	    	
	    }
	    
	    /**
	     * Get all authorized user
	     */
	    public static Set<String> getUsers(){
	    	try {
				return FLOW.getCredentialDataStore().keySet();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    	return null;
	    }
}

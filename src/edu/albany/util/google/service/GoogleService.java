package edu.albany.util.google.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.gson.JsonObject;

import edu.albany.util.google.GoogleOAuthClient;
import edu.albany.util.google.domain.GoogleToken;
import edu.albany.util.mysql.service.SqlService;
import edu.albany.util.mysql.utility.DatabaseUtils;

@Service("googleService")
public class GoogleService {

	final static Logger logger = Logger.getLogger(GoogleService.class);
	@Autowired
	public DatabaseUtils db;
	
	@Resource
	private SqlService Service;
	
	public Credential getToken(String username) throws Exception {
		db.setURL("itm3");
		String sql = "SELECT username, access_token,refresh_token,expire_time from user where username = '"+username+"'";
		List<Map<String, Object>> tokens = db.getJdbcTemplate().queryForList(sql);
		if(tokens.isEmpty() || tokens.get(0).get("refresh_token") == null) return null;
		Credential credential = GoogleOAuthClient.getNewCredential(username);
		credential.setAccessToken(tokens.get(0).get("access_token").toString());
	    credential.setRefreshToken(tokens.get(0).get("refresh_token").toString());
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    Date date = (Date)formatter.parse(tokens.get(0).get("expire_time").toString());
	    credential.setExpirationTimeMilliseconds(date.getTime());
		return credential;
	}
	 
	public void saveToken(Credential crd, String username, String access_token, String refresh_token, long expire_time) throws IOException, SQLException {
		 Drive service = GoogleOAuthClient.getDriveService(crd);
		 About about = service.about().get().setFields("user").execute();
		 String email = about.getUser().getEmailAddress();
		 long expiresIn = System.currentTimeMillis()+ (expire_time-60)*1000;
		 //save to mysql: insert or update if exists
		 db.setURL("itm3");
		 String sql = "insert into user(username,password,google_email,access_token,refresh_token,expire_time)"
				 + " values('"+username+"','"+username+"', '"+email+"','"+access_token+"','"+refresh_token+"',from_unixtime("+expiresIn+"/1000))";
		 db.getJdbcTemplate().update(sql);
		 sql = "update user set google_email='"+email+"',access_token='"+access_token+"',refresh_token='"+refresh_token+"',expire_time=from_unixtime("+expiresIn+"/1000) "
				 + " where username='"+username+"'";
		 db.getJdbcTemplate().update(sql);
	 }
	
	public String addNote(Credential crd, String projectid,String threadid, String username, String viewids, String title, String scaffolds, String coauthors, String plain_content){
    	//create doc in google drive
    	Drive service = GoogleOAuthClient.getDriveService(crd);
    	try{
    		String rootFolder = createRootFolder(service);
    		
    		File fileMetadata = new File();
    		fileMetadata.setName(title);
    		fileMetadata.setParents(Collections.singletonList(rootFolder));
    		fileMetadata.setMimeType("application/vnd.google-apps.document");
    		File file = null;
    		if(plain_content.equals("") && scaffolds.equals("")){
    				file = service.files().create(fileMetadata)
       		             .setFields("id")
       		             .execute();
    		}
    		else{
    			java.io.File filePath = new java.io.File("tmp_"+new Date().getTime()+".html");
    			if(plain_content.equals("") && !scaffolds.equals("")){
            		FileUtils.writeStringToFile(filePath, createScaffoldHTML(scaffolds));
	    		}
	    		else if(!plain_content.equals("") && scaffolds.equals("")){
	    			FileUtils.writeStringToFile(filePath, createPlainContentHTML(plain_content));
	    		}
	    		else{
	    			FileUtils.writeStringToFile(filePath, createComplexHTML(scaffolds,plain_content));
	    		}
    			FileContent fc = new FileContent("text/html",filePath);
                file = service.files().create(fileMetadata,fc)
   		             .setFields("id")
   		             .execute();
        		filePath.delete();
    		} 
    		
			//save to local using thread
    		NoteSaveThread nst = new NoteSaveThread(db, crd, file.getId(), projectid, threadid, username,viewids,title,coauthors,"");
    		nst.start();
			JsonObject res = new JsonObject();
			res.addProperty("docid", file.getId());
	        return res.toString();
		}
		catch(Exception e){
	        e.printStackTrace();
    	}
    	return null;
    }
	
	/**
     * create root folder in google drive if not exist
     * @param service
     * @param dbname
     * @return
     * @throws IOException
     */
    private String createRootFolder(Drive service) throws IOException {
    	String rootId = "";
    	FileList folders = service.files().list()
    			.setQ("name = 'ITM3' and mimeType='application/vnd.google-apps.folder' and trashed=false")
    			.setFields("files(id, name)").execute();
    	if(folders.getFiles().size() != 0){
    		rootId = folders.getFiles().get(0).getId();
    	}
    	else{
    		File fileMetadata = new File();
    		fileMetadata.setName("ITM3");
    		fileMetadata.setMimeType("application/vnd.google-apps.folder");
            File file = service.files().create(fileMetadata)
		             .setFields("id")
		             .execute();
            rootId = file.getId();
    	}
    	return rootId;
    }
    
    private String createScaffoldHTML(String scaffolds){
    	StringBuffer fsb = new StringBuffer();
		fsb.append("<html><head><meta content=\"text/html; charset=UTF-8\" http-equiv=\"content-type\"></head>")
			.append("<body style=\"background-color: #ffffff; max-width: 451.4pt\">");
		if(!scaffolds.equals("")){
			String[] sfs = scaffolds.split(",");
			for(int i = 0; i< sfs.length; i++){
				fsb.append("<p>").append("[").append(sfs[i]).append("&nbsp;-&nbsp;&nbsp;-&nbsp;]").append("</p>");
			}
		}
		fsb.append("</body></html>");
		return fsb.toString();
    }
    
    private String createPlainContentHTML(String plain_content){
    	StringBuffer fsb = new StringBuffer();
		fsb.append("<html><head><meta content=\"text/html; charset=UTF-8\" http-equiv=\"content-type\"></head>")
			.append("<body style=\"background-color: #ffffff; max-width: 451.4pt\">");
		if(!plain_content.equals("")){
				fsb.append("<p>")
				.append(plain_content)
				.append("</p>");
		}
		fsb.append("</body></html>");
		return fsb.toString();
    }
    
    private String createComplexHTML(String scaffolds, String plain_content){
    	StringBuffer fsb = new StringBuffer();
		fsb.append("<html><head><meta content=\"text/html; charset=UTF-8\" http-equiv=\"content-type\"></head>")
			.append("<body style=\"background-color: #ffffff; max-width: 451.4pt\">");
		if(!scaffolds.equals("")){
			String[] sfs = scaffolds.split(",");
			for(int i = 0; i< sfs.length; i++){
				fsb.append("<p>").append("[").append(sfs[i]).append("&nbsp;-&nbsp;&nbsp;-&nbsp;]").append("</p>");
			}
		}
		if(!plain_content.equals("")){
			fsb.append("<p>")
			.append(plain_content)
			.append("</p>");
		}
		fsb.append("</body></html>");
		return fsb.toString();
    }
    
    public boolean refreshNote(Credential crd, String docid){
    	Drive service = GoogleOAuthClient.getDriveService(crd);
		try{
			File file = service.files().get(docid).setFields("id,name,permissions").execute();
			OutputStream outputStream = new ByteArrayOutputStream();
			service.files().export(docid, "text/html").executeMediaAndDownloadTo(outputStream);
			String content = outputStream.toString();
			outputStream.close();
			if(content.indexOf("https://docs.google.com/drawings/image") != -1){
				//{"allowFileDiscovery":false,"id":"anyoneWithLink","kind":"drive#permission","role":"reader","type":"anyone"}
				boolean exist = false;
				for(Permission p: file.getPermissions()){
					if(p.getId().equals("anyoneWithLink")){
						exist = true;
						break;
					}
				}
				if(!exist){
					Permission newPermission = new Permission();
		        	newPermission.setType("anyone");
		        	newPermission.setRole("reader");
		        	service.permissions().create(docid, newPermission).execute();
				}
			}
			int idx = content.indexOf("<p ");
			content = content.substring(idx);
			//content = content.replaceFirst("<body ", "<div ");
			idx = content.indexOf("</body>");
			content = content.substring(0,idx);
			db.setURL("localdb");
			String sql = "update note_table set title = '"+file.getName()+"', content = '"+content+"' "
					+ " where doc_id = '"+docid+"'";
			db.getJdbcTemplate().update(sql);
		}
		catch(Exception e){
	        e.printStackTrace();
	        return false;
    	}
    	return true;
    }
}

package edu.albany.util.google.service;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.Permission;

import edu.albany.util.google.GoogleOAuthClient;
import edu.albany.util.google.domain.GoogleToken;
import edu.albany.util.mysql.utility.DatabaseUtils;

public class NoteSaveThread extends Thread {

	private DatabaseUtils db;
	private String docid;
	private Credential crd;
	private String projectid;
	private String threadid;
	private String username;
	private String viewids;
	private String title;
	private String coauthors;
	private String parent_noteid;
	private JsonBatchCallback<Permission> callback = new JsonBatchCallback<Permission>() {
	    @Override
	    public void onFailure(GoogleJsonError e,
	                          HttpHeaders responseHeaders)
	            throws IOException {
	        // Handle error
	        System.err.println(e.getMessage());
	    }

	    @Override
	    public void onSuccess(Permission permission,
	                          HttpHeaders responseHeaders)
	            throws IOException {
	        System.out.println("Created Permission ID: " + permission.getId());
	    }
	};
	
	public NoteSaveThread(DatabaseUtils db, Credential crd, String docid, String projectid, String threadid, String username, String viewids, String title, String coauthors, String parent_noteid){
		this.db = db;
		this.docid = docid;
		this.crd = crd;
		this.projectid = projectid;
		this.threadid = threadid;
		this.username = username;
		this.viewids = viewids;
		this.title = title;
		this.coauthors = coauthors;
		this.parent_noteid = parent_noteid;
	}
	
	public void run(){
		try{
			db.setURL("localdb");
			//generate note_id
			long current = System.nanoTime();
			String note_id = String.valueOf(current) +"-"+ String.valueOf(new Random().nextInt(10)+1);
			String sql = "insert into note_table(note_id, title, content, create_time, doc_id, status) "
					+ "values('"+note_id+"','"+title+"','',NOW(),'"+docid+"','active');";
			db.getJdbcTemplate().update(sql);
			//save to author_note
	        //coauthor
	        sql = "insert into author_note(author_id, note_id) select str_id, "+note_id+" from user where username='"+username+"' limit 1";
	        db.getJdbcTemplate().update(sql);
	        if(!coauthors.equals("")){
		        String[] list = coauthors.split(",");
		    	StringBuffer sb = new StringBuffer();
		    	sb.append("insert ignore into author_note values");
		    	for(int i = 0; i< list.length; i++){
		    		sb.append("("+list[i]+","+note_id+")").append(",");
		    	}
		    	sb.deleteCharAt(sb.length()-1);
		    	db.getJdbcTemplate().update(sb.toString());
	        }
	        
			//save to thread_note
			sql = "insert into thread_note(project_id,thread_id,note_id, count, create_time) values("+projectid+","+threadid+",'"+note_id+"',0,NOW())";
			System.out.println(sql);
			db.getJdbcTemplate().update(sql);
			if(parent_noteid.equals("")){
				//new note
				StringBuffer sf = new StringBuffer();
				String[] vids = viewids.split(",");
				for(int i = 0; i< vids.length; i++){
					sf.append("(").append(note_id).append(",").append(vids[i]).append("),");
				}
				sf.deleteCharAt(sf.length()-1);
				sql = "insert into view_note(note_id, view_id) values"+sf.toString();
				db.getJdbcTemplate().update(sql);
			}
			else{
				//buildson need to modify...
				sql = "insert into view_note(note_id, view_id) select "+note_id+", viewid from view_note where noteid = "+parent_noteid;
				db.getJdbcTemplate().update(sql);
				sql = "insert into note_note(from_note_id, to_note_id, linktype) values("+parent_noteid+","+note_id+",'buildson')";
				db.getJdbcTemplate().update(sql);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}

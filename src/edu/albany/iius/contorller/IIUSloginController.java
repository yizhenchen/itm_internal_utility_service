package edu.albany.iius.contorller;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.albany.util.mysql.dao.Community;
import edu.albany.util.mysql.service.CommunityService;
import edu.albany.util.mysql.service.UserService;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;
import edu.albany.util.mysql.utility.ResponseStatus;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import com.sun.jersey.core.util.MultivaluedMapImpl;


@RequestMapping("/")
public class IIUSloginController {

	@Autowired
	public DatabaseUtils db;
	
	@Resource
	public UserService userService;
	
	@Resource
	public CommunityService communityService;
	
	
	//get kf communites and local communites
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public CustomResponse get(HttpServletRequest request,
			@RequestParam(value="username") String userName,
			@RequestParam(value="password") String password
		){
		db.setURL("itm3");
		// get user 
		
		if(!userService.checkUserByUserNamePassword(userName, password)){// if user not appear in database,then validation fails
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.failure);
			cr.setDesc("Wrong username or password");
			cr.setObj(true);
			return cr;
		}else{
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("Valid User");
			cr.setObj(true);
			HttpSession session = request.getSession();
			session.setAttribute("username",userName);
			return cr;
		}
		
		}

}

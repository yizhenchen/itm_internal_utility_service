package test;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import edu.albany.util.mysql.controller.IndexController;
import edu.albany.util.mysql.service.ITM3Operation;
import edu.albany.util.mysql.service.KFSearchService;
import edu.albany.util.mysql.service.SqlService;
import edu.albany.util.mysql.service.UserService;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;
import edu.albany.util.mysql.utility.ResponseUtil;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class IndexControllerTest extends TestCase{ 
	
	public IndexController controller = new IndexController();
	public DatabaseUtils db =  new DatabaseUtils();

	public ResponseUtil responseUtil  = new ResponseUtil();
	public UserService userService  = new UserService();
	private SqlService Service  = new SqlService();
	private ITM3Operation ITM3Service = new ITM3Operation();
	private KFSearchService searchService  = new KFSearchService();
	public SqlService sqlservice=new SqlService();
	public void setup(){
		db.setURL("itm3");
		controller.db=db;
		userService.db = db;
		Service.db = db;
		ITM3Service.db = db;
		searchService.db = db;
		sqlservice.db =db;
		searchService.Service=sqlservice;

		controller.responseUtil =responseUtil;
		controller.userService=userService;
		controller.Service = Service;
		controller.ITM3Service = ITM3Service;
		controller.searchService =searchService;
	}
	@Test  
	public void test1(){ 
		 setup();
		CustomResponse cr =controller.UserValidation("20171029163219826664");
		System.out.println(cr.getDesc());
		
		CustomResponse cr2 =controller.UserValidation("201711029163219826664");
		System.out.println(cr.getDesc());
    } 
	
	@Test  
	public void test2(){ 
		 setup();
		CustomResponse cr =controller.validatePermission(null,null,555,"20171203230109303977");
		System.out.println(cr.getDesc());
    }  
	
	
	@Test  
	public void test3(){ 
		 setup();
		CustomResponse cr =controller.validatePermission(null,null,1);
		System.out.println(cr.getDesc());
    } 
	
	
	@Test  
	public void test4(){ 
		 setup();
		CustomResponse cr =controller.getAccessCode(null,null,1);
		System.out.println(cr.getDesc());
    } 
	
	/*
	 * @RequestParam(value="database", required=false) String database,
			@RequestParam(value="view_ids", required=false) String view_ids,
			@RequestParam(value="project_ids", required=false) String project_ids,
			@RequestParam(value="from_date", required=false) String from_date,
			@RequestParam(value="to_date", required=false) String to_date,
			@RequestParam(value="category", required=false) String category,
			@RequestParam(value="keywords", required=false) String keywords,
			@RequestParam(value="isExact", required=false) String isExact,
			@RequestParam(value="users", required=false) String users
	 */
	@Test  
	public void test5(){ 
		setup();
		CustomResponse cr =controller.search(null,null,
				"localdb",
				"[]",
				"[]",
				"",
				"",
				"0",
				"Test",
				"0",
				"[]"
				);
		System.out.println(cr.getDesc());
    } 
	
	public static void main(String arg[]){
		TestSuite suite = new TestSuite();
		suite.addTestSuite(test.IndexControllerTest.class);
		junit.textui.TestRunner.run(suite);	
		System.out.println("Test Case ");
	}
}

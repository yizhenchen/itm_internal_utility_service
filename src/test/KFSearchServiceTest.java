package test;

import org.junit.Test;

import edu.albany.util.mysql.service.KFSearchService;
import edu.albany.util.mysql.service.SqlService;
import edu.albany.util.mysql.utility.DatabaseUtils;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class KFSearchServiceTest extends TestCase{ 

	public SqlService sqlService = new SqlService();
	public KFSearchService service = new KFSearchService();
	public DatabaseUtils db =  new DatabaseUtils();
	
	@Test  
	public void test(){  
    	service.db=db;
    	db.setURL("localdb");
    	sqlService.db=db;
    	service.Service=sqlService;
    	service.search(
    			"", 
    			"", 
    			"", 
    			"", 
    			"0", 
    			"0", 
    			"Test", 
    			"");
    }  
	
	
	@Test  
	public void test2(){  
    	service.db=db;
    	db.setURL("localdb");
      	sqlService.db=db;
    	service.Service=sqlService;
		//String view_ids, String project_ids, String from_date, String to_date, 
		//String isExact, String category, String keywords, String users )
    	service.search(
    			"", 
    			"", 
    			"", 
    			"", 
    			"1", 
    			"0", 
    			"Human body", 
    			"");
    }  
	public static void main(String arg[]){
		TestSuite suite = new TestSuite();
		suite.addTestSuite(test.KFSearchServiceTest.class);
		junit.textui.TestRunner.run(suite);	
		System.out.println("Test Case ");
	}
}

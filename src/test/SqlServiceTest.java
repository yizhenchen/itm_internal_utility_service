package test;

import org.junit.Test;

import edu.albany.util.mysql.service.SqlService;
import edu.albany.util.mysql.utility.DatabaseUtils;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class SqlServiceTest extends TestCase{ 
	
	public SqlService service = new SqlService();
	public DatabaseUtils db =  new DatabaseUtils();
	
	@Test  
	public void test1(){  
    	service.db=db;
    	db.setURL("itm3");
    	service.get("select * from user");
    }  
	
	@Test  
	public void test2(){  
    	service.db=db;
    	db.setURL("itm3");
    	service.getWithOutJson("select * from user");
    }  
	
	
	@Test  
	public void test3(){  
    	service.db=db;
    	db.setURL("itm3");
    	service.getWithOutJson("select * from user");
    }  
	
	
	@Test  
	public void test4(){  
    	service.db=db;
    	db.setURL("itm3");
    	service.getWithOutJson("select * from user");
    }  
	public static void main(String arg[]){
		TestSuite suite = new TestSuite();
		suite.addTestSuite(test.SqlServiceTest.class);
		junit.textui.TestRunner.run(suite);	
		System.out.println("Test Case ");
	}
}

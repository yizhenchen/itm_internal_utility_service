package test;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import edu.albany.util.mysql.controller.IndexController;
import edu.albany.util.mysql.controller.kf6loginController;
import edu.albany.util.mysql.service.CommunityService;
import edu.albany.util.mysql.service.ITM3Operation;
import edu.albany.util.mysql.service.KFSearchService;
import edu.albany.util.mysql.service.SqlService;
import edu.albany.util.mysql.service.UserService;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;
import edu.albany.util.mysql.utility.ResponseUtil;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class kf6loginControllerTest extends TestCase{ 
	
	public kf6loginController controller = new kf6loginController();
	public DatabaseUtils db =  new DatabaseUtils();

	public ResponseUtil responseUtil  = new ResponseUtil();
	public UserService userService  = new UserService();
	private CommunityService Service  = new CommunityService();
	private ITM3Operation ITM3Service = new ITM3Operation();
	private KFSearchService searchService  = new KFSearchService();
	public SqlService sqlservice=new SqlService();
	public void setup(){
		db.setURL("itm3");
		controller.db=db;
		userService.db = db;
		Service.db = db;
		searchService.Service=sqlservice;

		controller.communityService =Service;
	}
	@Test  
	public void test1(){ 
		 setup();
		controller.get(
				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1N2MwNjdkYTIwNTc5MDVjMjkwMDgzYzgiLCJpYXQiOjE1MTY1OTMwNTAsImV4cCI6MTUxNjYxMTA1MH0.S2hrrcCCCvbMZxkpBHIrVHScBA6PZ7E9TtucYT8drLI", 
				"http://kf6.rit.albany.edu/");
    } 
	
	@Test  
	public void test2(){ 
		 setup();
		 controller.getkfCommunities("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1N2MwNjdkYTIwNTc5MDVjMjkwMDgzYzgiLCJpYXQiOjE1MTY1OTMwNTAsImV4cCI6MTUxNjYxMTA1MH0.S2hrrcCCCvbMZxkpBHIrVHScBA6PZ7E9TtucYT8drLI", 
		"http://kf6.rit.albany.edu/");
    }  
	
	
	@Test  
	public void test3(){ 
		 setup();
		 controller.getLocalCommunities("jzhang1@albany.edu", "itm");
    } 
	
	

	
	public static void main(String arg[]){
		TestSuite suite = new TestSuite();
		suite.addTestSuite(test.kf6loginControllerTest.class);
		junit.textui.TestRunner.run(suite);	
		System.out.println("Test Case ");
	}
}

package test;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import edu.albany.util.mysql.controller.ITM3OperationController;
import edu.albany.util.mysql.controller.IndexController;
import edu.albany.util.mysql.dao.RequestToGetThreadView;
import edu.albany.util.mysql.dao.RequestToITM3;
import edu.albany.util.mysql.dao.requestToGetUserToken;
import edu.albany.util.mysql.service.ITM3Operation;
import edu.albany.util.mysql.service.KFSearchService;
import edu.albany.util.mysql.service.SqlService;
import edu.albany.util.mysql.service.UserService;
import edu.albany.util.mysql.utility.CustomResponse;
import edu.albany.util.mysql.utility.DatabaseUtils;
import edu.albany.util.mysql.utility.ResponseUtil;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ITM3OperationControllerTest extends TestCase{ 
	
	public ITM3OperationController controller = new ITM3OperationController();
	public DatabaseUtils db =  new DatabaseUtils();

	public ResponseUtil responseUtil  = new ResponseUtil();
	public UserService userService  = new UserService();
	private SqlService Service  = new SqlService();
	private ITM3Operation ITM3Service = new ITM3Operation();
	private KFSearchService searchService  = new KFSearchService();
	public SqlService sqlservice=new SqlService();
	public void setup(){
		db.setURL("itm3");
		controller.db=db;
		userService.db = db;
		Service.db = db;
		ITM3Service.db = db;
		searchService.db = db;
		sqlservice.db =db;
		searchService.Service=sqlservice;
		controller.ITM3Service =ITM3Service;
		controller.responseUtil =responseUtil;
		controller.userService=userService;
	}
	@Test  
	public void test1(){ 
		 setup();
		 RequestToITM3 request = new RequestToITM3();
		 request.setCommunity_id("57c052ea2057905c29008319");
		 request.setKf_user_id("57c1b6d52057905c29008471");
		 CustomResponse cr =controller.getProject(request);
		 System.out.println(cr.getDesc());
		
    } 
	
	@Test  
	public void test2(){ 
		 setup();
		 RequestToITM3 request = new RequestToITM3();
		 request.setCommunity_id("57c052ea2057905c29008319");
		 request.setKf_user_id("581108ccfe0b3519295105b9");
		 CustomResponse cr =controller.getProject(request);
		 System.out.println(cr.getDesc());
    }  
	
	
	@Test  
	public void test3(){ 
		 setup();
		 RequestToGetThreadView rtt = new RequestToGetThreadView();
		 rtt.setCommunity_id("57c052ea2057905c29008319");
		 rtt.setThread_id("3");
		 controller.threadViewGet(rtt);
    } 
	
	
	@Test  
	public void test4(){ 
		 setup();
		 RequestToITM3 rti = new RequestToITM3();
		 rti.setCommunity_id("57c052ea2057905c29008319");
		 rti.setNote_id("5a4f945c3f124a2b8ead4e6f");
		 controller.getProjectByNoteId(rti);
    } 
	
	/*
	 * @RequestParam(value="database", required=false) String database,
			@RequestParam(value="view_ids", required=false) String view_ids,
			@RequestParam(value="project_ids", required=false) String project_ids,
			@RequestParam(value="from_date", required=false) String from_date,
			@RequestParam(value="to_date", required=false) String to_date,
			@RequestParam(value="category", required=false) String category,
			@RequestParam(value="keywords", required=false) String keywords,
			@RequestParam(value="isExact", required=false) String isExact,
			@RequestParam(value="users", required=false) String users
	 */
	@Test  
	public void test5(){ 
		setup();
		requestToGetUserToken request = new requestToGetUserToken();
		request.setCommunity_id("57c052ea2057905c29008319");
		request.setCommunity_url("http://kf6.rit.albany.edu/");
		request.setToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1N2MwNjdkYTIwNTc5MDVjMjkwMDgzYzgiLCJpYXQiOjE1MTY1OTMwNTAsImV4cCI6MTUxNjYxMTA1MH0.S2hrrcCCCvbMZxkpBHIrVHScBA6PZ7E9TtucYT8drLI");
		controller.getUserToken(request);
    } 
	
	public static void main(String arg[]){
		TestSuite suite = new TestSuite();
		suite.addTestSuite(test.ITM3OperationControllerTest.class);
		junit.textui.TestRunner.run(suite);	
		System.out.println("Test Case ");
	}
}

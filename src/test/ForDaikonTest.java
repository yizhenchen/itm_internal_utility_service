package test;

import org.apache.commons.lang.CharSet;
import org.apache.commons.lang.CharSetUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ForDaikonTest {
	public void test(CharSet charSet){
	    System.out.println("**CharSetDemo**");  
        String demoStr = "The quick brown fox jumps over the lazy dog.";  
        int count = 0;  
        for (int i = 0, len = demoStr.length(); i < len; i++) {  
            if (charSet.contains(demoStr.charAt(i))) {  
                count++;  
            }  
        }  
        System.out.println("count: " + count);  
	}
	public static void main(String arg[]){
		ForDaikonTest test =  new ForDaikonTest();
        CharSet charSet = CharSet.getInstance("aeiou");  
		test.test(charSet);
	}
}

package test;
import junit.framework.TestSuite;

public class DaikonTest {
	public static void main(String arg[]){
		TestSuite suite = new TestSuite();
		suite.addTestSuite(test.CommunityServiceTest.class);
		suite.addTestSuite(test.IndexControllerTest.class);
		suite.addTestSuite(test.ITM3OperationTest.class);
		suite.addTestSuite(test.KFSearchServiceTest.class);
		suite.addTestSuite(test.SqlServiceTest.class);
		suite.addTestSuite(test.UserServiceTest.class);
		junit.textui.TestRunner.run(suite);	
		System.out.println("Test Case ");
	}
}

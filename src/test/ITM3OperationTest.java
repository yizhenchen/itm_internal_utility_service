package test;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.junit.Test;

import edu.albany.util.mysql.dao.Community;
import edu.albany.util.mysql.dao.Project;
import edu.albany.util.mysql.service.ITM3Operation;
import edu.albany.util.mysql.utility.DatabaseUtils;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ITM3OperationTest extends TestCase{ 
	
	public ITM3Operation service = new ITM3Operation();
	public DatabaseUtils db =  new DatabaseUtils();
	
	@Test  
	public void test(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.checkNoteId("57c0746e2057905c290083f9");
    }  
	
	// List<Project> getProjectsByUserId(String user_id){
	
	@Test  
	public void test2(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.getProjectsByUserId("57c1b6d52057905c29008471");
    }
	
	@Test  
	public void test3(){  
    	service.db=db;
    	db.setURL("itm3");
    	service.getLocalDb("57c052ea2057905c29008319");
    }
	

	@Test  
	public void test4(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.getViewsByThreadId("61");
    }
	
	
	@Test  
	public void test5(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.getNotesInfoByViewIdAndAuthorID("57c4e0f82057905c290087f0","57d44bbcf2ab52ef08aeec1a");
    }
//getProjectByNotes
	
	@Test  
	public void test6(){  
    	service.db=db;
    	db.setURL("localdb");
    	List<String> ls_string  = new ArrayList<String>();
    	ls_string.add("59e41f452b2721248695eecd");
    	service.getProjectByNotes(ls_string);
    }
	
	//getUserByStrId
	
	@Test  
	public void test7(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.getUserByStrId("57c1b6d52057905c29008471");
    }
	
	@Test  
	public void test8(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.checkUser(
    			"ttt",
    			"https://kf6.ikit.org/",
    			"57c052ea2057905c29008319");
    }
	
	
	@Test  
	public void test9() throws ParseException{  
    	service.db=db;
    	db.setURL("localdb");
    	service.getUser(
    			"https://kf6.ikit.org/",
    			"ttt",
    			"57c052ea2057905c29008319");
    }
	
	
	@Test  
	public void test10(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.getToken(123);
    }
	
	@Test  
	public void test11(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.getToken(123);
    }
	
	
	@Test  
	public void test12(){  
    	service.db=db;
    	db.setURL("localdb");
    	service.getUserTokenByStrId("57c052ea2057905c29008319");
    }
	
	public static void main(String arg[]){
		TestSuite suite = new TestSuite();
		suite.addTestSuite(test.ITM3OperationTest.class);
		junit.textui.TestRunner.run(suite);	
		System.out.println("Test Case ");
	}
	
}

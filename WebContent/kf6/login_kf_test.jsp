<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KF6 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../resource/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../resource/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../resource/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div id="login" class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b>KF6</b> login</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="https://itm.arcc.albany.edu/ITM3s/kf_project" method="post">
          <div class="form-group has-feedback">
					  <select id="kfurl" class="form-control">
                        <option value="https://kf6.ikit.org/">https://kf6.ikit.org/</option>
                           <option value="http://169.226.75.138:9000/">http://169.226.75.22:9000/</option>
                           <option value="http://kf6.rit.albany.edu:8080/">http://kf6.rit.albany.edu:8080/</option>
                      </select>
            </div>
          <div class="form-group has-feedback">
            <input id="token" name="token" type="text" class="form-control" placeholder="User Name">
              </div>
          <div class="form-group has-feedback">
            <input  id="community_id" type="text"  name="community_id" class="form-control" placeholder="Password">
                </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
            
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button id="submit" type="button" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

   

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->


    <div id="process" class="login-box">     
            <i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Processing ..... 
    </div>
    <!-- jQuery 2.1.4 -->
    <script src="../resource/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../resource/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="../resource/plugins/iCheck/icheck.min.js"></script>
    <script src="../resource/notify.js"></script>
   
  </body>
</html>